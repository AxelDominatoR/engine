------------------------------------------------------
--	Checkbox
------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cCheckbox( ... )
end

local interface = __holder__.__holder__

Class "cCheckbox" (interface.widgets.cWidget.cWidget)
function cCheckbox:__init(x,y,w,h,txt)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._text = txt
	self._value = 0
end

function cCheckbox:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].checkbox[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local border_c2 = xml_data.border_c2
	local border_c3 = xml_data.border_c3
	local border_c4 = xml_data.border_c4
	local frame_c1 = xml_data.frame_c1
	local frame_c2 = xml_data.frame_c2
	local frame_c3 = xml_data.frame_c3
	local cr = xml_data.corner_radius
	local text_c1 = xml_data.text_c1
	local text_c2 = xml_data.text_c2
	local font = graphics.import.font(xml_data.font,xml_data.font_size)

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	if (activeID == self.id) then
		graphics.setColor(border_c4)
		graphics.rectangle("fill",x,y,w,h,cr)
	else
		graphics.setColor(border_c2)
		graphics.rectangle("fill",x,y,w,h,cr)
	end

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x+lrtb,y+1,w-lrtb-lrtb,h-lrtb-1,cr)

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+1+lrtb,y+1+lrtb,w-2-lrtb-lrtb,h-2-lrtb-lrtb,cr)

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)

	if (clickID == self.id) then
		graphics.setColor(frame_c3)
		graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)
	elseif (focusID == self.id) then
		graphics.setColor(frame_c2)
		graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)
	end

	if (self._value) then
		graphics.setColor(frame_c1)
		graphics.rectangle("fill",x+4+lrtb,y+4+lrtb,w-8-lrtb-lrtb,h-8-lrtb-lrtb,cr)
	end

	graphics.setClearColor()
	graphics.setScissor()
end

function cCheckbox:mousereleased(x,y,button,istouch)
	if (button == 1) then
		clickID = 0
		dragID = 0
		self._value = not self._value
	end
end

function cCheckbox:getChecked()
	return self._value
end

function cCheckbox:setChecked(b)
	self._value = b == true or false
end
