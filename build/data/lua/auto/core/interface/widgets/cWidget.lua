---------------------------------------------------
-- Base class for all widgets
--
-- Every time a widget is created, it is given the id count+1.
-- We don't want to hold references to a widget directly for GC reasons. It will just lead to leaks.
-- This means use local to reference widgets directly, and for multi-scope usage reference the widget ID.
---------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cWidget( ... )
end

local interface = __holder__.__holder__

Class "cWidget"
function cWidget:__init(x,y,w,h)
	self.parentID = 0
	self._visible = true
	self._enabled = true
	self._need_draw = true
	self._draggable = true

	self.x = x
	self.y = y
	self.w = w
	self.h = h

	self.ox = 0
	self.oy = 0

	self.style_xml = core.interface.import.style()
	self.script = nil

	interface.registerWidget( self )
end

--[[
	Sets or gets parent widget.
	If parent is being set, then inherited values are recalculated
]]--
function cWidget:parent( parent )
	if ( parent == nil ) then
		return interface.widgets[ self.parentID ]
	else
		self.parentID = parent.id

		self.x = self.x + parent.x
		self.y = self.y + parent.y

		if (( self.script == nil ) and parent.script ) then
			self.script = parent.script
		end

		if (( self.style_xml == nil ) and parent.style_xml ) then
			self.style_xml = self.style_xml
		end
	end
end

function cWidget:getText()
	return self._text or ""
end

function cWidget:setText(txt)
	self._text = txt or ""
end

function cWidget:add(...)
 	if not (self.children) then
		self.children = {}
	end
	local p = {...}
	for i,child in ipairs(p) do
		child:parent( self )
		table.insert(self.children,child.id)
	end
	return self
end

function cWidget:childForEach(func,...)
	if (self.children) then
		for i,id in ipairs(self.children) do
			local child = interface.widgets[ id ]
			if (child) then
				func(child,...)
				child:childForEach(func,...)
			end
		end
	end
end

function cWidget:visible(b)
	if (b == nil) then
		if not (self._visible) then
			return false
		end
		local parent = self
		repeat
			if (interface.widgets[parent.parentID]) then
				parent = interface.widgets[parent.parentID]
				if not (parent._visible) then
					return false
				end
			end
		until (parent.parentID == 0)
		return true
	else
		self._visible = b == true or false
	end
end

function cWidget:getDrag()
	if (self._draggable) then
		return self
	end
	local parent = self
	repeat
		if (interface.widgets[parent.parentID]) then
			parent = interface.widgets[parent.parentID]
			if (parent._draggable) then
				return parent
			end
		end
	until (parent.parentID == 0)
end

function cWidget:on_drag_update()

end

-- cWidget.top
-- Finds top parent, then re-registers the widget to give top draw order
function cWidget:top()
	local parent = self
	repeat
		if (interface.widgets[parent.parentID]) then
			parent = interface.widgets[parent.parentID]
		end
	until (parent.parentID == 0)
	interface.drawOrder_reinsert(parent)
	parent:childForEach(interface.drawOrder_reinsert)
end

function cWidget:topParent()
	local parent = self
	repeat
		if (interface.widgets[parent.parentID]) then
			parent = interface.widgets[parent.parentID]
		end
	until (parent.parentID == 0)
	return parent.id
end

function cWidget:find_by_alias(name)
	if (self._alias == name) then
		return self
	end
	local parent = self
	repeat
		if (interface.widgets[parent.parentID]) then
			parent = interface.widgets[parent.parentID]
			if (parent._alias == name) then
				return parent
			end
		end
	until (parent.parentID == 0)
	if (parent.children) then
		local node = parent.children
		local stack,size = {},0
		while (true) do
			for i,child_id in ipairs(node) do
				local child = interface.widgets[ child_id ]
				if (child) then
					if (child._alias == name) then
						return child
					end
					if (child.children) then
						size = size + 1
						stack[size] = child.children
					end
				end
			end
			if (size > 0) then
				node = stack[size]
				stack[size] = nil
				size = size - 1
			else
				return
			end
		end
	end
end

function cWidget:__call(...)
 	if not (self.children) then
		self.children = {}
	end
	local p = {...}
	for i,child in ipairs(p) do
		child.parentID = self.id
		table.insert(self.children,child.id)
	end
	return self
end

function cWidget:draw()
--[[ 	if (self.script and self.script.draw) then
		self.script.draw(self._alias)
	end ]]
end

function cWidget:update(dt,mouse_x,mouse_y)
	if (self.parentID ~= 0) then
		-- If have a parent, then remove if parent no longer exists
		local parent = interface.widgets[ self.parentID ]
		if not (parent) then
			self:delete()
		end
	end
	if (self.script and self.script.update) then
		self.script.update(self._alias,dt,mouse_x,mouse_y)
	end
end

function cWidget:insideXY(x,y)
	local x1 = self.x+self.ox
	local y1 = self.y+self.oy
	return x >= x1 and y >= y1 and x <= x1+self.w and y <= y1+self.h
end

function cWidget:checkCollision(other,x,y)
	local x1 = self.x+self.ox
	local y1 = self.y+self.oy
	local x2 = x or other.x+other.ox
	local y2 = y or other.y+other.oy
	return x1 < x2 + other.w and x1 + self.w > x2 and y1 < y2 + other.h and self.h + y1 > y2
end

function cWidget:checkInside(other)
	local x1 = self.x+self.ox
	local y1 = self.y+self.oy
	local x2 = other.x+other.ox
	local y2 = other.y+other.oy
	return x2 >= x1 and x2+other.w <= x1+self.w and y2 >= y1 and y2+other.h <= y1+self.h
end

function cWidget:clearScreen()
	graphics.setScissor(self.x+self.ox,self.y+self.oy,self.w,self.h)
	graphics.clear()
	graphics.setScissor()
end

function cWidget:clampScreen(toParent)
	if (toParent) then
		local parent = interface.widgets[ self.parentID ]
		if (parent) then
			self.x = self.x < parent.x and parent.x or self.x+self.w > parent.x+parent.w and parent.x+parent.w-self.w or self.x
			self.y = self.y < parent.y and parent.y or self.y+self.h > parent.y+parent.h and parent.y+parent.h-self.h or self.y
			return
		end
	end
	local w,h = graphics.getSize()
	self.x = self.x < 0 and 0 or self.x+self.w > w and w-self.w or self.x
	self.y = self.y < 0 and 0 or self.y+self.h > h and h-self.h or self.y
end

function cWidget:delete()
	interface.widgets[self.id] = nil
	self = nil
end

function cWidget:keypressed(key,scancode,aliases,isrepeat)
	if (self.script and self.script.keypressed) then
		self.script.keypressed(self._alias,key,scancode,aliases,isrepeat)
	end
end

function cWidget:keyreleased(key,scancode,aliases)
	if (self.script and self.script.keyreleased) then
		self.script.keyreleased(self._alias,key,scancode,aliases)
	end
end

function cWidget:mousepressed(x,y,button,istouch)
	if (button == 1) then
		self:top()
	elseif (button == 3) then
		local widget = self:getDrag()
		if (widget) then
			if (widget.on_drag) then
				widget:on_drag(x,y)
			end
			widget:top()
			dragID = widget.id
			dragX = x - widget.x+widget.ox
			dragY = y - widget.y+widget.oy
		end
	end
	if (self.on_mousepressed) then
		self:on_mousepressed(x,y,button,istouch)
	end
	if (self.script and self.script.mousepressed) then
		self.script.mousepressed(self._alias,x,y,button,istouch)
	end
end

function cWidget:mousereleased(x,y,button,istouch)
	if (button == 3) then
		dragID = 0
		if (self.on_drag_drop) then
			self:on_drag_drop(x,y)
		end
	end

	if (self.on_mousereleased) then
		self:on_mousereleased(x,y,button,istouch)
	end
	if (self.script and self.script.mousereleased) then
		self.script.mousereleased(self._alias,x,y,button,istouch)
	end
end

function cWidget:wheelmoved(x,y)
	if (self.on_wheelmoved) then
		self:on_wheelmoved(x,y)
	end
	if (self.script and self.script.wheelmoved) then
		self.script.wheelmoved(self._alias,x,y)
	end
end

function cWidget:textinput(text)
	if (self.script and self.script.textinput) then
		self.script.textinput(self._alias,text)
	end
end

function cWidget:focus(mouse_x,mouse_y)
	if (self.on_focus) then
		self:on_focus(mouse_x,mouse_y)
	end
	-- focus might need refactored
	self:childForEach(self.focus,mouse_x,mouse_y)
	if (self.script and self.script.focus) then
		self.script.focus(self._alias,mouse_x,mouse_y) -- TODO: add for children?
	end
end

function cWidget:focus_lost(mouse_x,mouse_y)
	if (self.on_focus_lost) then
		self:on_focus_lost(mouse_x,mouse_y)
	end
	self:childForEach(self.focus_lost,mouse_x,mouse_y)
	if (self.script and self.script.focus_lost) then
		self.script.focus_lost(self._alias,mouse_x,mouse_y) -- TODO: add for children?
	end
end

function cWidget:active_focus(mouse_x,mouse_y)
	if (self.on_active_focus) then
		self:on_active_focus(mouse_x,mouse_y)
	end
	self:childForEach(self.active_focus,mouse_x,mouse_y)
	if (self.script and self.script.active_focus) then
		self.script.active_focus(self._alias,mouse_x,mouse_y) -- TODO: add for children?
	end
end

function cWidget:active_focus_lost(mouse_x,mouse_y)
	if (self.on_active_focus_lost) then
		self:on_active_focus_lost(mouse_x,mouse_y)
	end
	self:childForEach(self.active_focus_lost,mouse_x,mouse_y)
	if (self.script and self.script.active_focus_lost) then
		self.script.active_focus_lost(self._alias,mouse_x,mouse_y) -- TODO: add for children?
	end
end

--[[
	Simple relative position adjustment
	pos format is: "top: n, bottom: n, right: n, left: n" where n is a number.
	Any position is optional and can be placed in any order.
	By declaring *both* left and right *and* omitting width, it will be autocalculated.
	Same for top and bottom with the height.
	Examples:
	<button w="100" h="25" pos="right: 10, top: 200">Test</button>
	will position button
	<button h="25" pos="right: 10, left: 20">Test</button>
	will position and resize button
	<button w="100" h="25" pos="right: 10, left: 20">Test</button>
	will position but *not* resize button, as width is declared. Position will default to left in this case
]]--
function cWidget:adjust_position( pos )
	-- outer boundaries to calculate new position from
	-- taken from parent, if available, otherwise use our render area size
	local ext_w, ext_h = 0, 0
	if ( self.parentID > 0 ) then
		local parent = interface.widgets[ self.parentID ]
		ext_w = parent.w
		ext_h = parent.h
	else
		ext_w, ext_h = graphics.getSize()
	end

	-- break pos into tokens and populate related variables
	-- positioning is performed during this stage as well
	local top, right, bottom, left = nil, nil, nil, nil
	local plist = pos and utils.str_explode( pos, ",") or {}
	for i=1, #plist do
		local pos_tokens = utils.str_explode( plist[ i ], ":")
		if ( pos_tokens[ 1 ] == "top" ) then
			top = pos_tokens[ 2 ]
			self.y = top
		elseif ( pos_tokens[ 1 ] == "right" ) then
			right = ext_w - pos_tokens[ 2 ]
			self.x = right - self.w
		elseif ( pos_tokens[ 1 ] == "bottom" ) then
			bottom = ext_h - pos_tokens[ 2 ]
			self.y = bottom - self.h
		elseif ( pos_tokens[ 1 ] == "left" ) then
			left = pos_tokens[ 2 ]
			self.x = left
		end
	end

	-- if the conditions for autoresize are met, then recalculate dimensions
	if ( right and left and ( self.w < 1 )) then
		w = math.abs( right - left )
		self.w = w
		self.x = left
	end

	if ( top and bottom and ( self.h < 1 )) then
		h = math.abs( bottom - top )
		self.h = h
		self.y = top
	end
end
