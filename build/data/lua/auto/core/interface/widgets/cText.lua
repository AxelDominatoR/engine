-----------------------------------------------------------------------------
-- Text
-----------------------------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cText( ... )
end

local interface = __holder__.__holder__

Class "cText" (interface.widgets.cWidget.cWidget)
function cText:__init(x,y,w,h,txt)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._text = txt
	self._highlight = false
end

function cText:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].frame[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local text_c1 = xml_data.text_c1
	local text_c2 = xml_data.text_c2
	local font = graphics.import.font(xml_data.font,xml_data.font_size)

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	if (self._text and self._text ~= "") then
		local old_font = graphics.getFont()
		graphics.setFont(font)

		graphics.setColor(self._highlight and focusID == self.id and text_c2 or text_c1)
		graphics.text(self._text,x,y)

		graphics.setFont(old_font)
	end

	-- default
	graphics.setClearColor()
	graphics.setScissor()
end
