------------------------------------------------------
--	Frame
------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cFrame( ... )
end

local interface = __holder__.__holder__

Class "cFrame" (interface.widgets.cWidget.cWidget)
function cFrame:__init(x,y,w,h,title)
	self.inherited[ 1 ].__init(self,x,y,w,h)
end

function cFrame:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].frame[ 1 ][ 1 ]
	local lrb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local border_c2 = xml_data.border_c2
	local border_c3 = xml_data.border_c3
	local border_c4 = xml_data.border_c4
	local frame_c1 = xml_data.frame_c1
	local cr = xml_data.corner_radius
	local text_c1 = xml_data.text_c1
	local text_c2 = xml_data.text_c2
	local font = graphics.import.font(xml_data.font,xml_data.font_size)

	local t = self._text and 30 or lrb
	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	if (activeID == self.id) then
		graphics.setColor(border_c4)
		graphics.rectangle("fill",x,y,w,h,cr)
	else
		graphics.setColor(border_c2)
		graphics.rectangle("fill",x,y,w,h,cr)
	end

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x+lrb,y+1,w-lrb-lrb,h-lrb-1,cr)

	if (self._text and self._text ~= "") then
		local old_font = graphics.getFont()
		graphics.setFont(font)

		graphics.setColor(text_c1)
		graphics.text(self._text,self.x+self.w/2-(self._text:len()*(font.size/2))/2-2,self.y+5)

		graphics.setFont(old_font)
	end

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+lrb+1,y+t+1,w-lrb-lrb-2,h-lrb-t-2,cr)

	graphics.setColor(frame_c1)
	graphics.rectangle("fill",x+2+lrb,y+2+t,w-4-lrb-lrb,h-4-lrb-t,cr)

	if (self.image) then
		self.image:draw(x+w/2-self.image.width/2,y+h/2-self.image.height/2)
	end

	graphics.setClearColor()
	graphics.setScissor()
end
