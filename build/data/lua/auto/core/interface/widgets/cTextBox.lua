-----------------------------------------------------------------------------
-- TextBox
-----------------------------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cTextBox( ... )
end

local interface = __holder__.__holder__

Class "cTextBox" (interface.widgets.cWidget.cWidget)
function cTextBox:__init(x,y,w,h,txt)
	self.inherited[ 1 ].__init(self,x,y,w,h)
	self._text = txt or ""
end

function cTextBox:draw(dt)
	local xml_data = self.style_xml.data.xml[ 1 ].textbox[ 1 ][ 1 ]
	local lrb = xml_data.border_size
	local border_c2 = xml_data.border_c2
	local border_c4 = xml_data.border_c4
	local frame_c1 = xml_data.frame_c1
	local cr = xml_data.corner_radius
	local text_c1 = xml_data.text_c1

	local font = graphics.import.font(xml_data.font,xml_data.font_size)

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	if (activeID == self.id) then
		graphics.setColor(border_c4)
		graphics.rectangle("fill",x,y,w,h,cr)
	else
		graphics.setColor(border_c2)
		graphics.rectangle("fill",x,y,w,h,cr)
	end

	graphics.setColor(frame_c1)
	graphics.rectangle("fill",x+lrb,y+1,w-lrb-lrb,h-lrb-1,cr)

	graphics.setScissor(x,y,w,h)

	local old_font = graphics.getFont()
	graphics.setFont(font)

	graphics.setColor(text_c1)
	graphics.text(captureID == self.id and self._text.."|" or self._text,x+2,y+2)

	graphics.setFont(old_font)

	-- default
	graphics.setClearColor()
	graphics.setScissor()
end

function cTextBox:textinput(txt)
	self._text = self._text .. ffi.string(txt)
	self._need_draw = true
	self.inherited[ 1 ].textinput(self,txt)
end

function cTextBox:keypressed(key,scancode,aliases,isrepeat)
	if (key == "Backspace" and isrepeat ~= false) then
		self._text = self._text:sub(1,-2)
		self._need_draw = true
	end
	self.inherited[ 1 ].keypressed(self,key,scancode,aliases,isrepeat)
end
