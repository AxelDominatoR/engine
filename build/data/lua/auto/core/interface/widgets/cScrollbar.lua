------------------------------------------------------
--	Scrollbar
------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cScrollbarA( ... )
end

local interface = __holder__.__holder__

Class "cScrollbarA" (interface.widgets.cWidget.cWidget)
function cScrollbarA:__init(x,y,w,h,vert,target_id)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._vertical = vert
	self.targetID = target_id
end

function cScrollbarA:update(dt,mouse_x,mouse_y)
	self.inherited[ 1 ].update(self,dt,mouse_x,mouse_y)
	if (dragID == self.id) then
		local target = widgets[ self.targetID ]
		if (target) then
			local parent = widgets[ self.parentID ]
			if (parent) then
				local diffX = parent.x+parent.w - self.x
				local diffY = parent.y - self.y
				target:childForEach(function(child)
					if not (self._vertical) then
						child.ox = diffX + diffX * ( self.w / target.w )
					else
						child.oy = diffY + diffY * ( self.h / target.h )
					end
				end)
			end
		elseif (type(self.targetID) == "string") then
			local widget = self:find_by_alias(self.targetID)
			if (widget) then
				self.targetID = widget.id
			end
		end
	end
end

function cScrollbarA:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].scrollbar[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local border_c2 = xml_data.border_c2
	local border_c3 = xml_data.border_c3
	local border_c4 = xml_data.border_c4
	local frame_c1 = xml_data.frame_c1
	local frame_c2 = xml_data.frame_c2
	local frame_c3 = xml_data.frame_c3
	local cr = xml_data.corner_radius

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ] and widgets[ widgets[ self.parentID ].parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	graphics.setColor(border_c2)
	graphics.rectangle("fill",x,y,w,h,cr)

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x+lrtb,y+1,w-lrtb-lrtb,h-lrtb-1,cr)

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+1+lrtb,y+1+lrtb,w-2-lrtb-lrtb,h-2-lrtb-lrtb,cr)

	graphics.setColor(frame_c1)
	graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)

	if (clickID == self.id) then
		graphics.setScissor(x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb)
		graphics.setClearColor(unpack(frame_c3))
		graphics.clear()
	elseif (focusID == self.id) then
		graphics.setScissor(x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb)
		graphics.setClearColor(unpack(frame_c2))
		graphics.clear()
	end

	graphics.setClearColor()
	graphics.setScissor()
end

function cScrollbarA:mousepressed(x,y,button,istouch)
	if (button == 1) then
		self:top()
		if (self._draggable) then
			self:top()
			dragID = self.id
			if not (self._vertical) then
				dragX = x - self.x
				dragY = -1
			else
				dragY = y - self.y
				dragX = -1
			end
		end
	end
	--focusID = self.id
	self.inherited[ 1 ].mousepressed(self,x,y,button,istouch)
end

function cScrollbarA:mousereleased(x,y,button,istouch)
	if (button == 1) then
		clickID = 0
	end
	dragID = 0
	self.inherited[ 1 ].mousereleased(self,x,y,button,istouch)
end

function cScrollbarA:wheelmoved(x,y)
	self.inherited[ 1 ].wheelmoved(self,x,y)
	local parent = widgets[ self.parentID ]
	if (parent) then
		self._need_draw = true
		if (self._vertical) then
			self.y = self.y - y * 4
			if (self.y+self.oy < parent.y+parent.oy) then
				self.y = parent.y+parent.oy
			elseif (self.y+self.oy+self.h > parent.y+parent.oy+parent.h) then
				self.y = parent.y+parent.oy+parent.h-self.h
			end
		else
			self.x = self.x - y * 4
			if (self.x+self.ox < parent.x+parent.ox) then
				self.x = parent.x+parent.ox
			elseif (self.x+self.ox+self.w > parent.x+parent.ox+parent.w) then
				self.x = parent.x+parent.ox+parent.w-self.w
			end
		end
		local target = widgets[ self.targetID ]
		if (target) then
			local parent = widgets[ self.parentID ]
			if (parent) then
				local diffX = parent.x+parent.w - self.x
				local diffY = parent.y - self.y
				target:childForEach(function(child)
					if not (self._vertical) then
						child.ox = diffX + diffX * ( self.w / target.w )
					else
						child.oy = diffY + diffY * ( self.h / target.h )
					end
				end)
			end
		end
	end
end

Class "cScrollbarB" (cWidget)
function cScrollbarB:__init(x,y,w,h,vert)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._vertical = vert
end

function cScrollbarB:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].scrollbar[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local cr = xml_data.corner_radius

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x,y,w,h,cr)

	-- default
	graphics.setClearColor()
	graphics.setScissor()
end
