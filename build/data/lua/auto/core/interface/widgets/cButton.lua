------------------------------------------------------
--	Button
------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cButton( ... )
end

local interface = __holder__.__holder__

Class "cButton" (interface.widgets.cWidget.cWidget)
function cButton:__init(x,y,w,h,txt)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._text = txt
end

function cButton:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].button[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local border_c2 = xml_data.border_c2
	local border_c3 = xml_data.border_c3
	local border_c4 = xml_data.border_c4
	local frame_c1 = xml_data.frame_c1
	local frame_c2 = xml_data.frame_c2
	local frame_c3 = xml_data.frame_c3
	local cr = xml_data.corner_radius
	local text_c1 = xml_data.text_c1
	local text_c2 = xml_data.text_c2
	local font = graphics.import.font(xml_data.font,xml_data.font_size)

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	if (activeID == self.id) then
		graphics.setColor(border_c4)
		graphics.rectangle("fill",x,y,w,h,cr)
	else
		graphics.setColor(border_c2)
		graphics.rectangle("fill",x,y,w,h,cr)
	end

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x+lrtb,y+1,w-lrtb-lrtb,h-lrtb-1,cr)

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+1+lrtb,y+1+lrtb,w-2-lrtb-lrtb,h-2-lrtb-lrtb,cr)

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)

	if (clickID == self.id) then
		graphics.setColor(frame_c3)
		graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)
	elseif (focusID == self.id) then
		graphics.setColor(frame_c2)
		graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)
	end

	graphics.setClearColor()

	if (self._text and self._text ~= "") then
		local old_font = graphics.getFont()
		graphics.setFont(font)

		graphics.setColor(text_c1)
		graphics.text(self._text,x+w/2-(self._text:len()*(font.size/2))/2-2,y+h/2-font.size/2-2)

		graphics.setFont(old_font)
	end

	graphics.setScissor()
end
