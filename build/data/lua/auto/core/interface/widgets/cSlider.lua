------------------------------------------------------
--	Slider
------------------------------------------------------

getmetatable(this).__call = function( self, ... )
	return self.cSliderA( ... )
end

local interface = __holder__.__holder__

Class "cSliderA" (interface.widgets.cWidget.cWidget)
function cSliderA:__init(x,y,w,h,vert)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._vertical = vert
	self._min = 0
	self._max = 1
	self._value = 0
	self._step = "0.0"
end

function cSliderA:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].slider[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local border_c2 = xml_data.border_c2
	local border_c3 = xml_data.border_c3
	local border_c4 = xml_data.border_c4
	local frame_c1 = xml_data.frame_c1
	local frame_c2 = xml_data.frame_c2
	local frame_c3 = xml_data.frame_c3
	local cr = xml_data.corner_radius
	local text_c1 = xml_data.text_c1
	local text_c2 = xml_data.text_c2
	local font = graphics.import.font(xml_data.font,xml_data.font_size)

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ] and widgets[ widgets[ self.parentID ].parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	graphics.setColor(border_c2)
	graphics.rectangle("fill",x,y,w,h,cr)

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x+lrtb,y+1,w-lrtb-lrtb,h-lrtb-1,cr)

	graphics.setColor(border_c3)
	graphics.rectangle("fill",x+1+lrtb,y+1+lrtb,w-2-lrtb-lrtb,h-2-lrtb-lrtb,cr)

	graphics.setColor(frame_c1)
	graphics.rectangle("fill",x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb,cr)

	if (clickID == self.id) then
		graphics.setScissor(x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb)
		graphics.setClearColor(unpack(frame_c3))
		graphics.clear()
	elseif (focusID == self.id) then
		graphics.setScissor(x+2+lrtb,y+2+lrtb,w-4-lrtb-lrtb,h-4-lrtb-lrtb)
		graphics.setClearColor(unpack(frame_c2))
		graphics.clear()
	end

	graphics.setClearColor()
	graphics.setScissor()
end

function cSliderA:on_drag_update()
	-- calculate slider value
	local parent = widgets[ self.parentID ]
	if (parent) then
		local normalized
		if (self._vertical) then
			local y1 =self.y+self.oy+self.h/2
			local y2 = parent.y+parent.oy
			normalized = utils.normalize(y1,y2,y2+parent.h,self._min,self._max)

			normalized = tonumber(string.format("%"..self._step.."f",normalized))

			self.y = utils.denormalize(normalized,y2,y2+parent.h,self._min,self._max)-self.h/2
		else
			local x1 = self.x+self.ox+self.w/2
			local x2 = parent.x+parent.ox
			normalized = utils.normalize(x1,x2,x2+parent.w,self._min,self._max)

			normalized = tonumber(string.format("%"..self._step.."f",normalized))

			self.x = utils.denormalize(normalized,x2,x2+parent.w,self._min,self._max)-self.w/2
		end

		self._value = normalized
	end
end

function cSliderA:mousepressed(x,y,button,istouch)
	if (button == 1) then
		self:top()
		if (self._draggable) then
			self:top()
			dragID = self.id
			if not (self._vertical) then
				dragX = x - self.x
				dragY = -1
			else
				dragY = y - self.y
				dragX = -1
			end
		end
	end
end

function cSliderA:mousereleased(x,y,button,istouch)
	if (button == 1) then
		clickID = 0
		dragID = 0
	end
end

function cSliderA:getValue()
	return self._value
end

function cSliderA:clampScreen(toParent)
	if (toParent) then
		local parent = widgets[ self.parentID ]
		if (parent) then
			self.x = self.x+self.w/2 < parent.x and parent.x-self.w/2 or self.x+self.w > parent.x+parent.w+self.w/2 and parent.x+parent.w-self.w/2 or self.x
			self.y = self.y+self.h/2 < parent.y and parent.y-self.h/2 or self.y+self.h > parent.y+parent.h+self.h/2 and parent.y+parent.h-self.h/2 or self.y
			return
		end
	end
	local w,h = graphics.getSize()
	self.x = self.x < 0 and 0 or self.x+self.w > w and w-self.w or self.x
	self.y = self.y < 0 and 0 or self.y+self.h > h and h-self.h or self.y
end

Class "cSliderB" (cWidget)
function cSliderB:__init(x,y,w,h,vert)
	self.inherited[ 1 ].__init(self,x,y,w,h)

	self._vertical = vert
end

function cSliderB:draw()
	local xml_data = self.style_xml.data.xml[ 1 ].slider[ 1 ][ 1 ]
	local lrtb = xml_data.border_size
	local border_c1 = xml_data.border_c1
	local border_c2 = xml_data.border_c2
	local cr = xml_data.corner_radius

	local x,y,w,h = self.x+self.ox,self.y+self.oy,self.w,self.h

	local parent = widgets[ self.parentID ]
	if (parent) then
		graphics.setScissor(parent.x+parent.ox,parent.y+parent.oy,parent.w,parent.h)
	end

	graphics.setColor(border_c2)
	graphics.rectangle("fill",x,y,w,h,cr)

	graphics.setColor(border_c1)
	graphics.rectangle("fill",x+lrtb,y+1,w-lrtb-lrtb,h-lrtb-1,cr)

	-- default
	graphics.setClearColor()
	graphics.setScissor()
end
