A cross-platform 64-bit Lua OpenGL/SDL Framework

Based partly on https://github.com/Planimeter/lgameframework

## Install
### Windows
**Microsoft Visual C++ 2010 SP1 Redistributable Package (x64)**  
https://www.microsoft.com/en-us/download/details.aspx?id=13523

**Visual C++ Redistributable for Visual Studio 2015**
https://www.microsoft.com/en-us/download/details.aspx?id=48145

#### Libraries
* [luajit-2](https://github.com/LuaJIT/LuaJIT) LuaJIT 2
* [lphysicsfs](https://icculus.org/pipermail/physfs/2012-October/001056.html) PhysicsFS virtual file system library
* [lsdl](https://www.libsdl.org/download-2.0.php) SDL 2.0
* OpenGL
* [lkazmath](https://github.com/Kazade/kazmath) Math Library
* [ldevil](http://openil.sourceforge.net/download.php)  
* [lfreetype](https://www.freetype.org/download.html)  Freetype Font
* [lsdl_sound](https://www.icculus.org/SDL_sound/)   SDL Sound
* [lopenal](https://openal.org/downloads/) OpenAL Sound
* [lassimp](http://assimp.sourceforge.net/main_downloads.html) Asset Import Library
* [expat](https://github.com/libexpat/libexpat) XML Parsing Library
* [lua_extensions](https://bitbucket.org/revo_lucas/lua_extensions) LFS, Marshal, and more!
* [libogg/libvorbis] (https://xiph.org/downloads/) Ogg Vorbis
* [pthread] (https://luapower.com/pthread) FFI bindings for POSIX threads

### Linux
* sudo apt-get install lua5.1-dev
* sudo apt-get install luajit
* sudo apt-get install libphysfs-dev
* sudo apt-get install libdevil-dev
* sudo apt-get install libsdl2-dev
* sudo apt-get install chipmunk-dev
* sudo apt-get install libassimp-dev
* sudo apt-get install libfreetype6-dev
* sudo apt-get install libvorbis-dev
* sudo apt-get install libopenal-dev

#### Kazmath:
```
	sudo add-apt-repository ppa:kazmath-developers/ppa
	sudo apt-get update
```	
	OR
```	
	git clone git@github.com:Kazade/kazmath.git
	(Change CMakeLists and set KAZMATH_BUILD_LUA_WRAPPER and KAZMATH_BUILD_GL_UTILS ON and the rest OFF)
	cd kazmath
	mkdir build
	cd build
	cmake .. -DBUILD_SHARED_LIBS=YES
	make
	sudo make install
```
#### Nuklear:
```
	git clone git@bitbucket.org:teamepicstalker/nuklear.git
	cd nuklear
	mkdir build
	cd build
	cmake ..
	make
	sudo make install
```	
#### Expat:
```
	git clone git@github.com:libexpat/libexpat.git
	cd libexpat
	mkdir build
	cd build
	cmake ..
	make
	sudo make install
```
#### Lua Lanes:
```
	git clone https://github.com/capr/multigit luapower
	cd luapower
	./mgit clone https://github.com/luapower/luapower-repos
	./mgit clone lanes
	
	Put core.so in bin/lanes
```
##NOTES:

Luajit-2 needs to be built with -pthread