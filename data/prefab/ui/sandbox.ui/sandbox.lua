local nk = nuklear

local tests_list = nil

function generate_sandbox_list( ctx, node, data, depth )
	if not ( tests_list ) then
		tests_list = {}
		filesystem.fileForEach("lua/auto/tests", true, {"lua"},
			function ( path, fname, fullpath )
				local test = fname:sub( 1, -5 )
				table.insert( tests_list, test )
			end
		)
	end

	for i, v in ipairs( tests_list ) do
		if ( nk.nk_button_label( ctx, v ) == 1 ) then
-- do something?
		end
	end
end

function on_close(ctx,node,data,depth)
	if (node.id == "sandbox") then
		nk.nk_window_close(ctx,node.id)
		core.interface.import.prefab.remove(data.__alias)
	end
end

function button_label_press(ctx,node,data,depth)
	local p = data["button_color1"]
	if (p) then
		p.color.r = math.random(1,255)
		p.color.g = math.random(1,255)
		p.color.b = math.random(1,255)
	end
end