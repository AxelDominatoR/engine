-----------------------
-- Stream
-----------------------
-- TODO: Fix isStreamed

streamBufferSize = 32 * 1024

ffi.cdef [[
typedef struct
{
	uint32_t chunkID;
	uint32_t chunkSize;
	uint32_t format;
	uint32_t subchunk1ID;
	uint32_t subchunk1Size;
	int16_t  audioFormat;
	int16_t  numChannels;
	uint32_t sampleRate;
	uint32_t byteRate;
	int16_t  blockAlign;
	int16_t  bitsPerSample;
	uint32_t subchunk2ID;
	uint32_t subchunk2Size;
} RiffWaveHeader;
]]

local RiffWaveHeader = ffi.new("RiffWaveHeader")

-----------------------
-- OggVorbis to PHYSFS callbacks
-----------------------
OggVorbisCallbacks = ffi.new("ov_callbacks")
OggVorbisCallbacks.read_func = function(buffer,initial,length,data)
	local file = ffi.cast("PHYSFS_File *",data)
	local ret = physfs.PHYSFS_read(file,buffer,initial,length)
	if (ret <= 0) then
		return 0
	end
	return ret
end
OggVorbisCallbacks.seek_func = function(data,off,whence)
	local file = ffi.cast("PHYSFS_File *",data)
	local length = whence == 1 and physfs.PHYSFS_tell(file)+off or whence == 2 and physfs.PHYSFS_fileLength(file)+off or off
	if (physfs.PHYSFS_seek(file,length) == 0) then
		return -1
	end
	return 0
end 
OggVorbisCallbacks.close_func = function(data)
	local file = ffi.cast("PHYSFS_File *",data)
	physfs.PHYSFS_close(file)
	return 0
end 
OggVorbisCallbacks.tell_func = function(data)
	local file = ffi.cast("PHYSFS_File *",data)
	return physfs.PHYSFS_tell(file)
end

function on_game_start()
	CallbackSet("framework_exit",on_exit)
end

function on_exit()
	OggVorbisCallbacks.read_func:free()
	OggVorbisCallbacks.seek_func:free()
	OggVorbisCallbacks.close_func:free()
	OggVorbisCallbacks.tell_func:free()
end

------------------------------
-- Class cBitStream
------------------------------
Class "cBitStream"
function cBitStream:__init(filename,isStreamed)
	if not (self:openStream(filename)) then 
		return
	end
	self.isStreamed = isStreamed
	if (self.bitsPerSample == 8) then 
		if (self.channels == 1) then
			self.format = AL.AL_FORMAT_MONO8
		elseif (self.channels == 2) then 
			self.format = AL.AL_FORMAT_STEREO8
		else
			printe("cBitStream: Unsupported number of channels (%s)",self.channels)
			return
		end
	elseif (self.bitsPerSample == 16) then 
		if (self.channels == 1) then
			self.format = AL.AL_FORMAT_MONO16
		elseif (self.channels == 2) then 
			self.format = AL.AL_FORMAT_STEREO16
		else
			printe("cBitStream: Unsupported number of channels (%s)",self.channels)
			return
		end
	else
		printe("cBitStream: Unsupported number of bits per sample (%s)",self.bitsPerSample)
		return
	end
	if (isStreamed) then
		self.pStreamBuffer = ffi.new("char[?]",streamBufferSize)
		self.pStreamBufferLength = streamBufferSize
		self:readStream()
	else
		self.pStreamBuffer = ffi.new("char[?]",self.size)
		self.pStreamBufferLength = self.size
		local sz = self:readStream()
		if (sz ~= self.size) then
			printe("cBitStream: Sound size mismatch: %s",filename)
		end
	end
end

function cBitStream:openStream(filename)
	
	local file = physfs.PHYSFS_openRead(filename)
	
	if (file == nil) then
		printe("audio_thread.lua: Failed to load %s",filename)
		return false
	end
	
	local buffer = ffi.new("char[?]",4)
	if (physfs.PHYSFS_read(file,buffer,1,4) == 4) then
		physfs.PHYSFS_seek(file,0)
		self.fileType = ffi.string(buffer,4)
		if (self.fileType == "OggS") then
			local oggVorbisFile = ffi.new("OggVorbis_File")
			ffi.gc(oggVorbisFile,function(oggVorbisFile) return vorbis.ov_clear(oggVorbisFile) end)

			jit.off() -- C callback to Lua, compilation not allowed
			local success = vorbis.ov_open_callbacks(file,oggVorbisFile,nil,0,OggVorbisCallbacks)
			jit.on()
			
			if (success == 0) then
				local info = vorbis.ov_info(oggVorbisFile,-1)
				self.bitsPerSample = 16
				self.channels = info.channels
				self.size = vorbis.ov_pcm_total(oggVorbisFile,-1) * 2 * self.channels
				self.frequency = info.rate
				self.data = oggVorbisFile
				return true
			end
		elseif (self.fileType == "RIFF") then
			ffi.gc(file,function(file) return physfs.PHYSFS_close(file) end)
			if (physfs.PHYSFS_read(file,RiffWaveHeader,1,ffi.sizeof(RiffWaveHeader)) == ffi.sizeof(RiffWaveHeader)) then
				assert(RiffWaveHeader.format == 0x45564157,"cBitStream: unrecognized format")
				assert(RiffWaveHeader.subchunk1ID == 0x20746D66,"cBitStream: unrecognized subchunk1ID")
				assert(RiffWaveHeader.subchunk2ID == 0x61746164,"cBitStream: unrecognized subchunk2ID")
				assert(RiffWaveHeader.audioFormat == 1,"cBitStream: unrecognized audio format")
				self.data = file
				self.size = RiffWaveHeader.subchunk2Size
				self.bitsPerSample = RiffWaveHeader.bitsPerSample
				self.channels = RiffWaveHeader.numChannels
				self.frequency = RiffWaveHeader.sampleRate
				return true
			end
		end
	end
	printe("cBitStream: unrecognized file type for %s",filename)
	return false
end

function cBitStream:readStream()
	if (self.fileType == "RIFF") then 
		return physfs.PHYSFS_read(self.data,self.pStreamBuffer,1,self.pStreamBufferLength)
	elseif (self.fileType == "OggS") then
		local pos = 0
		while (true) do
			local read = vorbis.ov_read(self.data,self.pStreamBuffer+pos,self.pStreamBufferLength-pos,0,2,1,nil)
			if (read == 0) then -- EOF
				break
			elseif (read < 0) then
				if (read == vorbis.OV_HOLE) then
					print("Vorbisfile encountered missing or corrupt data in the bitstream.")
				elseif (read == vorbis.OV_EBADLINK) then
					print("The given link exists in the Vorbis data stream, but is not decipherable due to garbage or corruption.")
				elseif (read == vorbis.OV_EINVAL) then
					print("Either an invalid argument, or incomplete initialized argument passed to libvorbisfile call.")
				end
				break
			end
			pos = pos + read
		end
		return pos
	end
end 

function cBitStream:getStreamPosition()
	if (self.fileType == "RIFF") then 
		return (physfs.PHYSFS_tell(self.data) - ffi.sizeof(RiffWaveHeader)) / (self.bitsPerSample / 8) / self.channels
	elseif (self.fileType == "OggS") then 
		local pos = vorbis.ov_pcm_tell(self.data)
		if (pos == vorbis.OV_EINVAL) then
			print("cBitStream:getStreamPosition - requested bitstream did not exist")
			return 0
		end
		return pos
	end
	return 0
end

function cBitStream:setStreamPosition(pos)
	if (self.fileType == "RIFF") then 
		physfs.PHYSFS_seek(self.data,pos * (self.bitsPerSample / 8) * self.channels + ffi.sizeof(RiffWaveHeader))
	elseif (self.fileType == "OggS") then
		local success = vorbis.ov_pcm_seek_lap(self.data,pos)
		if (success < 0) then
			if (success == vorbis.OV_ENOSEEK) then
				print("Bitstream is not seekable.")
			elseif (success == vorbis.OV_EINVAL) then 
				print("Invalid argument value; possibly called with an OggVorbis_File structure that isn't open.")
			elseif (success == vorbis.OV_EREAD) then
				print("A read from media returned an error.")
			elseif (success == vorbis.OV_EFAULT) then
				print("Internal logic fault; indicates a bug or heap/stack corruption.")
			elseif (success == vorbis.OV_EOF) then
				print("Indicates stream is at end of file immediately after a seek (making crosslap impossible as there's no preceeding decode state to crosslap).")
			elseif (success == vorbis.OV_EBADLINK) then
				print("Invalid stream section supplied to libvorbisfile, or the requested link is corrupt.")
			end
			return false
		end
	end
	return true
end

function cBitStream:closeStream()
	self.data = nil
end

function cBitStream:__gc()
	self.data = nil
end