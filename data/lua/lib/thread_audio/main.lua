local time = require("time")

set_finalizer(function(err,stk)
	--print("thread_audio: attempting to exit cleanly...")
	
	if (ALSoundManager) then
		ALSoundManager:cleanup()
		ALSoundManager = nil
	end
	
	if err and type( err) ~= "userdata" then
		-- no special error: true error
		print("thread_audio: error: "..tostring(err))
	elseif type(err) == "userdata" then
		-- lane cancellation is performed by throwing a special userdata as error
		print("thread_audio: after cancel")
	else
		-- no error: we just got finalized
		--print("thread_audio: finalized")
	end
end)
	
-- Main loop
while (cancel_test() ~= true) do
	local k,v = linda:receive(0,"thread-call")
	if (v ~= nil) then
		if (type(v) == "table" and type(v[ 1 ]) == "string" and type(v[ 2 ]) == "table" and _G[ v[ 1 ] ]) then
			_G[ v[ 1 ] ]( unpack(v[ 2 ]) )
		elseif (type(v) == "string" and _G[ v ]) then
			_G[ v ]()
		end
	end
	
	v = linda:get("updateListener")
	if (v ~= nil) then
		updateListener(unpack(v))
	end
	
	if (ALSoundManager and ALSoundManager.initialized) then
		for i,src in ipairs(ALSoundManager.sources) do
			src:update()
		end
	end
	time.sleep(0.015) -- 15ms
end