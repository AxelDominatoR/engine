local gfx = core.framework.graphics

function on_game_start()
	CallbackSet("framework_after_load", on_after_load )
	CallbackSet("framework_draw", on_draw )
end

function on_after_load()
	gfx.setBackgroundColor( 0x6E, 0x6E, 0x9D, 0xFF )

	local REMTreeRoot = subsystems.rem.data
	local rem = REMTreeRoot.prefab.ui["sandbox.ui"].sandbox
	core.interface.import.prefabrem("test", "prefab/ui/sandbox.ui", rem )
--	core.interface.import.prefab("test", "prefab/ui/sandbox.ui")
end

function on_draw( dt )
	gfx.shaders.useProgram("default2D")
	gfx.text( core.framework.getFPS(), 0, 0 )
end