--[[
	REM entry
--]]

local Node = core.extensions.node.Node
Entry = Node:inherit()

local Attribute = subsystems.rem.attribute.Attribute
local Dictionary = core.extensions.dictionary.Dictionary

function Entry:_init()
	Node._init( self )

-- _file is provided through args, so don't uncomment this!
--	self._file = nil

	self.id = ""

	self._type_decl = 0
	self._type = {}

	self._classes_decl = {}
	self._classes = Dictionary:new()

	self._ancestors_decl = {}
	self._ancestors = Dictionary:new()

	self._attributes = {}

	self.content = nil

	local mt = {}
	mt.__index = function( t, key )
		if ( Entry[ key ] ~= nil ) then
			return Entry[ key ]
		elseif ( t._attributes[ key ] ~= nil ) then
			return t._attributes[ key ]
		elseif ( t._children[ key ] ~= nil ) then
			return t._children[ key ]
		else
			if ( key == "content") then
				t.content = Attribute:new()
				return t.content
			end

			if t._type[ key ] ~= nil then
				return t._type[ key ]
			end

			for k, v in pairs( t._classes.items ) do
				if v[ key ] ~= nil then
					return v[ key ]
				end
			end

-- if rawget( t, _type ) ~= nil then

			for k, v in pairs( t._ancestors.items ) do
				if v[ key ] ~= nil then
					return v[ key ]
				end
			end

			print("REM Entry node key " .. key .. " not found!")
		end
	end

	setmetatable( self, mt )
end

function Entry:post_load()
-- TODO: decide if _type_decl is still needed after this
	if self._type_decl ~= nil then
		self._type = self._file:get_type( self._type_decl )
	end

	local classes = self._classes
-- TODO: decide if _classes_decl is still needed after this
	for k, v in pairs( self._classes_decl ) do
		local c = self._file:get_class( v, true )
		if c ~= nil then
			classes:add( c )
--			self:merge_attributes( c )
		else
			print("Class not found: " .. v )
		end
	end

	local ancestors = self._ancestors
-- TODO: decide if _ancestors_decl is still needed after this
	for _, v in pairs( self._ancestors_decl ) do
		local a = self._file:by_id( v, true )
		if a ~= nil then
			ancestors:add( a )
		else
			print("Ancestor not found: " .. v )
		end
	end

	for _, v in pairs( self._attributes ) do
		v:post_load( self )
	end

	if ( self.content ~= nil ) then
		self.content:post_load( self )
	end

	for _, v in pairs( self._children ) do
		v:post_load()
	end
end

-- optimizations
local stringsub = string.sub
local next = next
local tabs = utils.print_tabs

function Entry:merge_attributes( from, override )
	for k, v in pairs( from._attributes ) do
		self._attributes[ k ] = v
	end
end

function Entry:print_attributes( lvl )
	local data = {}
	local no_data = true
	for k, v in pairs( self._attributes ) do
		table.insert( data, k .. v:print())
		no_data = false
	end

	local info = {}
	table.insert( info, tabs( lvl ))
	if ( self._type_decl ~= nil ) then table.insert( info, self._type_decl ) end
	if ( self.id ~= "") then table.insert( info, "#" .. self.id ) end
	if ( next( self._ancestors ) ~= nil ) then table.insert( info, "[" .. table.concat( self._ancestors, ", ") .. "]") end
	table.insert( info, "(" .. self._classes:to_string() .. ")")
	if ( no_data == false ) then table.insert( info, "{" .. table.concat( data, ", ") .. "}") end
	if ( self.content.value ~= nil ) then
		print("Content------------------------")
		table.insert( info, self.content:print())
		print("------------------------Content")
	end

	print( table.concat( info, " "))
end

function Entry:export_to_nk()
	local res = {}

	res.id = self.id
	res._type = self._type_decl

	for k, v in pairs( self._attributes ) do
		if ( v.value ~= nil ) then
			res[ k ] = v.value
		else
			res[ k ] = v
		end
	end

	return res
end