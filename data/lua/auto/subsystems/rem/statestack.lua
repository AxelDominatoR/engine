--[[
	StateStack class
--]]

test = "BLA"

local Stack = core.extensions.stack.Stack
StateStack = Stack:inherit()

function StateStack:_init()
	Stack._init( self )

	self.state = self.default
	self.stack = {}
end

function StateStack:push( act, debug )
	if ( debug ~= nil ) then
		print("PUSH--------------------------")
		print( #self.stack )
		print( self.state.name )
	end

	table.insert( self.stack, act )
	self.state = act

	if ( self.state.on_push ~= nil ) then
		self.state.on_push()
	end

	if ( debug ~= nil ) then
		print( self.state.name )
		print( #self.stack )
		print("--------------------------PUSH")
	end
end

function StateStack:pop( debug )
	if ( debug ~= nil ) then
		print("POP---------------------------")
		print( #self.stack )
		print( self.state.name )
	end

	if ( self.state.on_pop ~= nil ) then
		self.state.on_pop()
	end

	table.remove( self.stack )
	self.state = self:peek() or self.default
	if ( debug ~= nil ) then
		print( self.state.name )
		print( #self.stack )
		print("---------------------------POP")
	end
end

function StateStack:swapontop( act, debug )
	if ( debug ~= nil ) then
		print("SOT---------------------------")
		print( #self.stack )
		print( self.state.name )
	end

	if ( self.state.on_pop ~= nil ) then
		self.state.on_pop()
	end

	table.remove( self.stack )
	table.insert( self.stack, act )
	self.state = act

	if ( self.state.on_push ~= nil ) then
		self.state.on_push()
	end

	if ( debug ~= nil ) then
		print( self.state.name )
		print( #self.stack )
		print("---------------------------SOT")
	end
end