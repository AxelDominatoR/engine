--[[
	REM node
--]]

local CoreNode = core.extensions.node.Node
Node = CoreNode:inherit()

function Node:_init()
	CoreNode._init( self )

	self.files = {}

	local mt = {}
	mt.__index = function( t, key )
		if ( Node[ key ] ~= nil ) then
			return Node[ key ]
		elseif ( t._children[ key ] ~= nil ) then
			return t._children[ key ]
		elseif ( t.files[ key ] ~= nil ) then
			return t.files[ key ]
		else
			print("REM node key " .. key .. " not found!")
		end
	end

	setmetatable( self, mt )
end

function Node:add_file( file )
	file.rem_node = self
	self.files[ file.name ] = file
end

function Node:post_load()
	for _, v in pairs( self._children ) do
		for _, r in pairs( v.files ) do
			r:post_load()
		end
		v:post_load()
	end
end

function Node:find_class( id )
-- TODO: bubble up to find classes
end

-- optimizations
local tabs = utils.print_tabs

function Node:print_tree( lvl )
	lvl = lvl or 0

	for _, v in pairs( self._children ) do
		print( tabs( lvl ) .. "[" .. v.name .. "]")
		for r, _ in pairs( v.files ) do
			print( tabs( lvl + 1 ) .. r .. ".rem")
		end
		v:print_tree( lvl + 1 )
	end
end
