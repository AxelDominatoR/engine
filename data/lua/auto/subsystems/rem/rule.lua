--[[
	Rule class
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
Rule = SimpleClass:inherit()

function Rule:_init()
	SimpleClass._init( self )

	self._attributes = {}
	self._selectors = {"testselector > test1", "testselector2.dfdf"}

	local mt = {}
	mt.__index = function( t, key )
		if ( Rule[ key ] ~= nil ) then
			return Rule[ key ]
		elseif ( t._attributes[ key ] ~= nil ) then
			return t._attributes[ key ]
		else
			print("REM rule key " .. key .. " not found!")
		end
	end

	setmetatable( self, mt )
end

function Rule:print()
	self:print_attributes( 0 )
end

-- optimizations
local stringsub = string.sub
local next = next
local tabs = utils.print_tabs

function Rule:print_attributes( lvl )
	local data = {}
	local no_data = true
	for k, v in pairs( self._attributes ) do
		table.insert( data, k .. v:print())
		no_data = false
	end

	local info = {}
	table.insert( info, tabs( lvl ))
	if ( self.id ~= nil ) then table.insert( info, "rule " .. self.id ) end
	if ( next( self._selectors ) ~= nil ) then table.insert( info, "(" .. table.concat( self._selectors, ", ") .. ")") end
	if ( no_data == false ) then table.insert( info, "{" .. table.concat( data, ", ") .. "}") end

	print( table.concat( info, " "))
end