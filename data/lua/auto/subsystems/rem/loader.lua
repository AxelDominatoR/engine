function on_game_start()
	CallbackSet("framework_init", on_init )
end

local REMFile = subsystems.rem.file.File
local REMNode = subsystems.rem.node.Node
local REMTreeRoot = nil

local function on_file_found( dir, fname, fullpath )
	local file = REMFile.loadfrom("data" .. fullpath, fname )
	local curr_node = REMTreeRoot

	for s in dir:gsplit("/" , true ) do
		if ( s ~= "") then
			if ( curr_node:has_child( s ) ~= true ) then
				curr_node:add_child( REMNode:new({ name = s }), s )
			end

			curr_node = curr_node:get_child( s )
		end
	end

	curr_node:add_file( file )
end

function on_init()
	local chrono_start = os.clock()
	print("Loading REM files...")

	REMTreeRoot = REMNode:new({ name = "ROOT"})
	subsystems.rem.data = REMTreeRoot

	filesystem.fileForEach("", true, {"rem"}, on_file_found )
	REMTreeRoot:post_load()

	local elapsed = os.clock() - chrono_start
	print( string.format("REM files loaded in: %.3f seconds", elapsed ))

--	REMTreeRoot:print_tree()

--	local t1 = REMTreeRoot.lua.auto.tests.test1
--	local t2 = REMTreeRoot.lua.auto.tests.test2
--	local t3 = REMTreeRoot.lua.auto.tests.test3

--	local t1 = REMFile.loadfrom("data/lua/auto/tests/test1.rem")

--[[
	print("T1--------------------------------------")
	t1:print()
	print("--------------------------------------T1")

	print( t1:by_id("test").prop2.value )
	print( t1:by_id("test3").prop3.value )
	print( t1:get_type("ttt").propr.value )
	print( t1:get_class("testclass").prop3.value )

	print( t1:by_id("test").propr.value )
	print( t1:by_id("test2").propr.value )
	print( t1:by_id("test3").propr.value )

	print("test4")

	print( t1:by_id("test4").prop.value )
	print( t1:by_id("test4").prop2.value )
	print( t1:by_id("test4").prop3.value )
	print( t1:by_id("test4").propr.value )

--]]

--	local ts = REMTreeRoot.prefab.ui["sandbox.ui"].sandbox
--	ts:print()

--	local ts2 = REMTreeRoot.prefab.ui["uitest.ui"].uitest
--	print("UITEST.UI--------------------------------------")
--	ts2:print()
--	print("--------------------------------------UITEST.UI")

--	local ts3 = REMTreeRoot.prefab.ui["sandbox.ui"].sandbox
--	local ts3 = REMFile.loadfrom("data/prefab/ui/sandbox.ui/sandbox.rem", "sandbox.rem")
--	ts3:post_load()
--	print("SANDBOX.UI-------------------------------------")
--	ts3:print()
--	print("-------------------------------------SANDBOX.UI")
end