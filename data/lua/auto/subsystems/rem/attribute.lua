--[[
	Attribute class
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
Attribute = SimpleClass:inherit()

AttributeType =
{
	STATIC    = 1,
	DYNAMIC   = 2,
	OBJECT    = 3
}

function Attribute:_init()
	SimpleClass._init( self )

	self.op = nil
	self.type = AttributeType.STATIC
	self.value = nil

	self._tokens = {}

	local mt = {}
	mt.__index = function( t, key )
		if ( Attribute[ key ] ~= nil ) then
			return Attribute[ key ]
		elseif ( t.type == AttributeType.OBJECT ) then
			if ( t.value[ key ] ~= nil ) then
				return t.value[ key ]
			end
		else
			if ( key == "op") then
				return "NOOP"
			end

			if ( key == "value") then
				return nil
			end

			print("REM Attribute key " .. key .. " not found!")
		end
	end

	setmetatable( self, mt )
end

function Attribute:set_type( t )
	if ( self.type ~= t ) then
		self.type = t
		local pv = self.value
		self.value = {}
		if ( pv ~= "") then
			table.insert( self.value, pv )
		end
	end
end

function Attribute:set_op( txt_op )
	self.op = txt_op
end

function Attribute:set_value( v )
	if ( self.type == AttributeType.DYNAMIC ) then
		table.insert( self.value, v )
	else
		self.value = v
	end
end

function Attribute:add_token( t )
	table.insert( self._tokens, t )
end

function Attribute:post_load( owner )
	if ( self.type == AttributeType.DYNAMIC ) then
		local res = {}

		local lastp = 1
		for i = 1, #self._tokens do
			table.insert( res, self.value[ i ])
			table.insert( res, owner[ self._tokens[ i ]].value )
			lastp = lastp + 1
		end

		table.insert( res, self.value[ lastp ])

		self.value = res
	end
end

function Attribute:to_string()
	return self.id
end

function Attribute:print()
	if ( self.type == AttributeType.DYNAMIC ) then
		print_table( self.value )
		print_table( self._tokens )
		return self.op .. "\"" .. table.concat( self.value ) .. "\""
	else
		print( self.value )
		return self.op .. "\"" .. self.value .. "\""
	end
end