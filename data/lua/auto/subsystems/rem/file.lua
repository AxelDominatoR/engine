--[[
	REM file
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
File = SimpleClass:inherit()

local Dictionary = core.extensions.dictionary.Dictionary

--[[
File =
{
	path = "",
	types = nil,
	classes = nil,
	rules = nil,
	data = nil

	rem_node
}
--]]

function File:_init()
	SimpleClass._init( self )

	self.rules = {}
end

function File.loadfrom( path, fname )
	print(" - " .. path )

	local df = File:new()
	df.name = fname:sub( 1, -5 )
	df.filename = fname
	df.path = path

	local f = assert( io.open( path, "rb"))
	local content = f:read("*all")
	f:close()

	--[[ Beginning of the Finite State Machine ]]--

	-- Character types --
	local CTYPE_DEFAULT                = 1
	local CTYPE_NUMBER                 = 2
	local CTYPE_UPPERALPHA             = 3
	local CTYPE_LOWERALPHA             = 4
	local CTYPE_WHITESPACE             = 5
	local CTYPE_TAB                    = 6
	local CTYPE_NEWLINE                = 7
	local CTYPE_MINUS                  = 8
	local CTYPE_PERCENT                = 9
	local CTYPE_BACKSLASH              = 10
	local CTYPE_PERIOD                 = 11
	local CTYPE_COMMA                  = 12
	local CTYPE_HASH                   = 13
	local CTYPE_COLON                  = 14
	local CTYPE_SEMICOLON              = 15
	local CTYPE_UNDERSCORE             = 16
	local CTYPE_QUOTES                 = 17
	local CTYPE_OPENROUNDBRACKET       = 18
	local CTYPE_CLOSEDROUNDBRACKET     = 19
	local CTYPE_OPENSQUAREBRACKET      = 20
	local CTYPE_CLOSEDSQUAREBRACKET    = 21
	local CTYPE_OPENCURLYBRACKET       = 22
	local CTYPE_CLOSEDCURLYBRACKET     = 23
	local CTYPE_OPERATOR               = 24

	-- Token types --
	local TTYPE_UNKNOWN     = 0
	local TTYPE_TYPE        = 1
	local TTYPE_ID          = 2
	local TTYPE_ANCESTOR    = 3
	local TTYPE_CLASS       = 4

	-- Status flags --
	local status =
	{
		attribute =
		{
			parsing_quotes = false
		},
		comment =
		{
			multiline = false
		},
		indent =
		{
			level = 0,
			prev_level = -1
		},
		token =
		{
			after_colon = false,
			type = TTYPE_UNKNOWN
		},
		type_class_is_class = false
	}

	local Entry = subsystems.rem.entry.Entry
	local Type = subsystems.rem.type.Type
	local Class = subsystems.rem.class.Class
	local Rule = subsystems.rem.rule.Rule

	local Attribute = subsystems.rem.attribute.Attribute
	local AttributeType = subsystems.rem.attribute.AttributeType

	local data = Entry:new({ id = "ROOT", _file = df })   -- data tree containing all of the parsed data
	data._parent = data                      -- Root node self-references itself as a parent
	local types = {}                         -- List of types
	local classes = Dictionary:new()         -- List of classes
	local rules = {}                         -- List of rules
	local curr_entry = data                  -- current node
	local curr_type_class = nil              -- current rule
	local curr_rule = nil                    -- current rule
	local curr_attribute_container = nil     -- current container for the attributes
	local curr_attribute = nil               -- current attribute
	local prev_char = nil                    -- previous character
	local string_buffer = {}                 -- buffer used when parsing strings
	local stringchar = string.char           -- local reference, optimization

	-- find_parent returns the proper parent for the current node
	function find_parent()
		local parent = curr_entry
		local si = status.indent
		local l = si.prev_level - si.level
		while l >= 0 do
			parent = parent._parent
			l = l - 1
		end

		return parent
	end

	-- States --
	local state_generic            = { name = "state_generic"}
	local state_level_count        = { name = "state_level_count"}
	local state_entry              = { name = "state_entry"}
	local state_entry_token        = { name = "state_entry_token"}
	local state_rule               = { name = "state_rule"}
	local state_type_class         = { name = "state_type_class"}
	local state_attribute_name     = { name = "state_attribute_name"}
	local state_attribute_op       = { name = "state_attribute_op"}
	local state_attribute_value    = { name = "state_attribute_value"}
	local state_literal            = { name = "state_literal"}
	local state_quotes             = { name = "state_quotes"}
	local state_string_token       = { name = "state_string_token"}
	local state_comment            = { name = "state_comment"}

	-- StateStack initialization --
	local StateStack = subsystems.rem.statestack.StateStack:new({ default = state_generic })

	--[[ State actions ]]--

	-- Ignore character
	local function stact_ignore() end

	-- Clear the string buffer
	local function stact_clear_buffer()
		string_buffer = {}
	end

	-- Save character in string_buffer
	local function stact_save_character( c )
		table.insert( string_buffer, stringchar( c ))
	end

--[[
	-- state_generic --
	Generic state (default)
--]]

	-- Parse alphanumeric
	local function stact_generic_alphanumeric( c )
		StateStack:push( state_attribute_name )
		return true
	end

	state_generic[ CTYPE_DEFAULT ] = stact_ignore
	state_generic[ CTYPE_NEWLINE ] =
		function()
			status.indent.level = 0
			StateStack:push( state_level_count )
		end
	state_generic[ CTYPE_MINUS ] =
		function( c )
			if ( prev_char == 45 ) then   -- 45 is "-"
				string_buffer = {}
				StateStack:push( state_comment )
			else
				table.insert( string_buffer, stringchar( c ))
			end
		end
	state_generic[ CTYPE_QUOTES ] =
		function()
			StateStack:push( state_quotes )
		end
	state_generic[ CTYPE_NUMBER ] = stact_generic_alphanumeric
	state_generic[ CTYPE_UPPERALPHA ] = stact_generic_alphanumeric
	state_generic[ CTYPE_LOWERALPHA ] = stact_generic_alphanumeric
	state_generic[ CTYPE_OPENSQUAREBRACKET ] =
		function()
			local entry = Entry:new({ _file = df })

			find_parent():add_child( entry )

			curr_entry = entry
			curr_attribute_container = entry
			curr_attribute = entry.content
			status.indent.prev_level = status.indent.level

			StateStack:push( state_entry )
		end
	state_generic[ CTYPE_OPENROUNDBRACKET ] =
		function()
			-- Start as a Type, change into Class later if required
			local type_class = Type:new()
			curr_type_class = type_class
			curr_attribute_container = type_class

			StateStack:push( state_type_class )
		end
	state_generic[ CTYPE_OPENCURLYBRACKET ] =
		function()
			local rule = Rule:new()
			curr_rule = rule
			curr_attribute_container = rule

			StateStack:push( state_rule )
		end

--[[
	-- state_level_count --
	Counts the number of tabs to determine the indentation level
--]]
	state_level_count[ CTYPE_DEFAULT ] =
		function( c )
			StateStack:pop()
			return true
		end
	state_level_count[ CTYPE_TAB ] =
		function( c )
			status.indent.level = status.indent.level + 1
		end

--[[
	-- state_entry --
	Reads an entry
--]]
	state_entry[ CTYPE_DEFAULT ] = stact_save_character
	state_entry[ CTYPE_WHITESPACE ] = stact_ignore
	state_entry[ CTYPE_UPPERALPHA ] =
		function()
			status.token.type = TTYPE_TYPE
			StateStack:push( state_entry_token )
			return true
		end
	state_entry[ CTYPE_LOWERALPHA ] =
		function()
			status.token.type = TTYPE_TYPE
			StateStack:push( state_entry_token )
			return true
		end
	state_entry[ CTYPE_COLON ] =
		function()
			status.token.after_colon = true
		end
	state_entry[ CTYPE_COMMA ] = stact_ignore
	state_entry[ CTYPE_HASH ] =
		function()
			if ( status.token.after_colon == false ) then
				status.token.type = TTYPE_ID
			else
				status.token.type = TTYPE_ANCESTOR
			end

			StateStack:push( state_entry_token )
		end
	state_entry[ CTYPE_PERIOD ] =
		function()
			status.token.type = TTYPE_CLASS
			StateStack:push( state_entry_token )
		end
	state_entry[ CTYPE_CLOSEDSQUAREBRACKET ] =
		function()
			string_buffer = {}
			status.token.after_colon = false
			StateStack:pop()
		end

--[[
	-- state_entry_token --
	Reads an entry token (type, id, classes, etc)
--]]
	state_entry_token[ CTYPE_NUMBER ] = stact_save_character
	state_entry_token[ CTYPE_UPPERALPHA ] = stact_save_character
	state_entry_token[ CTYPE_LOWERALPHA ] = stact_save_character
	state_entry_token[ CTYPE_UNDERSCORE ] = stact_save_character
	state_entry_token[ CTYPE_DEFAULT ] =
		function()
			if ( status.token.type == TTYPE_TYPE ) then
				curr_entry._type_decl = table.concat( string_buffer )
			elseif ( status.token.type == TTYPE_ID ) then
				curr_entry.id = table.concat( string_buffer )
			elseif ( status.token.type == TTYPE_ANCESTOR ) then
				table.insert( curr_entry._ancestors_decl, table.concat( string_buffer ))
			elseif ( status.token.type == TTYPE_CLASS ) then
				table.insert( curr_entry._classes_decl, table.concat( string_buffer ))
			end

			status.token.type = TTYPE_UNKNOWN
			string_buffer = {}
			StateStack:pop()

			return true
		end

--[[
	-- state_type_class --
	Reads a type or class
--]]
	state_type_class[ CTYPE_DEFAULT ] = stact_save_character
	state_type_class[ CTYPE_WHITESPACE ] = stact_ignore
	state_type_class[ CTYPE_PERIOD ] =
		function()
			-- If string_buffer is empty...
			if next( string_buffer ) == nil then
				local class = Class:new()
				curr_type_class = class
				curr_attribute_container = class
				status.type_class_is_class = true
			end
		end
	state_type_class[ CTYPE_CLOSEDROUNDBRACKET ] =
		function()
			curr_type_class.id = table.concat( string_buffer )
			string_buffer = {}

			if status.type_class_is_class == true then
				classes:add( curr_type_class )
			else
				table.insert( types, curr_type_class )
			end

			StateStack:pop()
		end

--[[
	-- state_rule --
	Reads a rule
--]]
	state_rule[ CTYPE_DEFAULT ] = stact_save_character
	state_rule[ CTYPE_WHITESPACE ] = stact_ignore
	state_rule[ CTYPE_CLOSEDCURLYBRACKET ] =
		function()
			curr_rule.id = table.concat( string_buffer )
			string_buffer = {}

			table.insert( rules, curr_rule )

			StateStack:pop()
		end

--[[
	-- state_attribute_name --
	Reads the name part of an attribute
--]]
	-- Shared ops when parsing end of attribute name
	local function stact_end_attribute_name()
		local name = table.concat( string_buffer )
		curr_attribute = Attribute:new()
		curr_attribute_container._attributes[ name ] = curr_attribute
		string_buffer = {}

-- As this is a complex state, only "attribute_value" is allowed to use the stack normally
		StateStack:swapontop( state_attribute_op )
	end

	state_attribute_name[ CTYPE_DEFAULT ] = stact_save_character
	state_attribute_name[ CTYPE_WHITESPACE ] = stact_end_attribute_name
	state_attribute_name[ CTYPE_OPERATOR ] =
		function()
			stact_end_attribute_name()
 			return true
		end

--[[
	-- state_attribute_op --
	Reads the operand of an attribute
--]]
	state_attribute_op[ CTYPE_DEFAULT ] = stact_save_character
	state_attribute_op[ CTYPE_WHITESPACE ] =
		function()
			curr_attribute:set_op( table.concat( string_buffer ))
			string_buffer = {}

-- As this is a complex state, only "attribute_value" is allowed to use the stack normally
			StateStack:swapontop( state_attribute_value )
		end

--[[
	-- state_attribute_value --
	Reads the value of an attribute
--]]
	-- Shared ops when parsing end of attribute value
	local function stact_end_attribute_value()
		if ( status.attribute.parsing_quotes == false ) then
			curr_attribute:set_value( table.concat( string_buffer ))
			string_buffer = {}
		end

		status.attribute.parsing_quotes = false
		curr_attribute = curr_entry.content

		StateStack:pop()
	end

	state_attribute_value[ CTYPE_DEFAULT ] = stact_save_character
	state_attribute_value[ CTYPE_QUOTES ] =
		function()
			status.attribute.parsing_quotes = true
			StateStack:push( state_quotes )
		end
	state_attribute_value[ CTYPE_COMMA ] = stact_end_attribute_value
	state_attribute_value[ CTYPE_NEWLINE ] =
		function()
			stact_end_attribute_value()
			return true
		end

--[[
	-- state_literal --
	Reads the next character as a literal (used to read special characters)
--]]
	state_literal[ CTYPE_DEFAULT ] =
		function( c )
			table.insert( string_buffer, stringchar( c ))
			StateStack:pop()
		end

--[[
	-- state_quotes --
	Reads text inside quotes, spanning multiple lines
--]]
	state_quotes.on_push =
		function()
			string_buffer = {}
		end
	state_quotes[ CTYPE_DEFAULT ] = stact_save_character
	state_quotes[ CTYPE_BACKSLASH ] =
		function()
			StateStack:push( state_literal )
		end
	state_quotes[ CTYPE_PERCENT ] =
		function()
			curr_attribute:set_type( AttributeType.DYNAMIC )
			curr_attribute:set_value( table.concat( string_buffer ))
			StateStack:push( state_string_token )
		end
	state_quotes[ CTYPE_QUOTES ] =
		function()
			curr_attribute:set_value( table.concat( string_buffer ))
			string_buffer = {}

			if ( status.attribute.parsing_quotes == false ) then
				curr_attribute = curr_entry.content
			end

			StateStack:pop()
		end

--[[
	-- state_string_token --
	Reads string token, ends when percent is found
--]]
	state_string_token.on_push = stact_clear_buffer
	state_string_token[ CTYPE_DEFAULT ] = stact_save_character
	state_string_token[ CTYPE_PERCENT ] =
		function()
			curr_attribute:add_token( table.concat( string_buffer ))
			string_buffer = {}
			StateStack:pop()
		end

--[[
	-- state_comment --
	Reads (and ignores) comments.
	Supports standard lua comment syntax.
--]]
	state_comment[ CTYPE_DEFAULT ] = stact_ignore
	state_comment[ CTYPE_OPENSQUAREBRACKET ] =
		function()
			if ( prev_char == 91 ) then   -- 91 is "["
				status.comment.multiline = true
			end
		end
	state_comment[ CTYPE_CLOSEDSQUAREBRACKET ] =
		function()
			if ( prev_char == 93 ) then   -- 93 is "]"
				status.comment.multiline = false
				StateStack:pop()
			end
		end
	state_comment[ CTYPE_NEWLINE ] =
		function()
			if ( status.comment.multiline == false ) then
				StateStack:pop()
			end
		end

	-- Main FSM loop --
	local stringbyte = string.byte
	local bcontent = { stringbyte( content, 1, #content )}
	for i = 1, #bcontent do
		local c = bcontent[ i ]
		local ctype = CTYPE_DEFAULT

		if     ( c == 32 )                                 then ctype = CTYPE_WHITESPACE
		elseif ( c >= 48 ) and ( c <= 57 )                 then ctype = CTYPE_NUMBER
		elseif ( c >= 97 ) and ( c <= 122 )                then ctype = CTYPE_LOWERALPHA
		elseif ( c >= 65 ) and ( c <= 90 )                 then ctype = CTYPE_UPPERALPHA
		elseif ( c == 44 )                                 then ctype = CTYPE_COMMA
		elseif ( c == 46 )                                 then ctype = CTYPE_PERIOD
		elseif ( c == 58 )                                 then ctype = CTYPE_COLON
		elseif ( c == 9 )                                  then ctype = CTYPE_TAB
		elseif ( c == 10 ) or ( c == 13 )                  then ctype = CTYPE_NEWLINE
		elseif ( c == 95 )                                 then ctype = CTYPE_UNDERSCORE
		elseif ( c == 35 )                                 then ctype = CTYPE_HASH
		elseif ( c == 45 )                                 then ctype = CTYPE_MINUS
		elseif ( c == 37 )                                 then ctype = CTYPE_PERCENT
		elseif ( c == 34 )                                 then ctype = CTYPE_QUOTES
		elseif ( c == 40 )                                 then ctype = CTYPE_OPENROUNDBRACKET
		elseif ( c == 41 )                                 then ctype = CTYPE_CLOSEDROUNDBRACKET
		elseif ( c == 91 )                                 then ctype = CTYPE_OPENSQUAREBRACKET
		elseif ( c == 93 )                                 then ctype = CTYPE_CLOSEDSQUAREBRACKET
		elseif ( c == 123 )                                then ctype = CTYPE_OPENCURLYBRACKET
		elseif ( c == 125 )                                then ctype = CTYPE_CLOSEDCURLYBRACKET
		elseif ( c == 92 )                                 then ctype = CTYPE_BACKSLASH
		elseif ( c == 43 ) or ( c == 45 ) or ( c == 61 )   then ctype = CTYPE_OPERATOR
		end

		local parse_again
		repeat
			if StateStack.state[ ctype ] == nil then
				parse_again = StateStack.state[ CTYPE_DEFAULT ]( c )
			else
				parse_again = StateStack.state[ ctype ]( c )
			end
		until parse_again ~= true

		prev_char = c
	end

	-- Fix files not ending with a newline. Ugly, but maybe more optimized.
	if ( prev_char ~= 10 ) or ( prev_char ~= 13 ) then
		local parse_again
		repeat
			if StateStack.state[ CTYPE_NEWLINE ] == nil then
				parse_again = StateStack.state[ CTYPE_DEFAULT ]( 10 )
			else
				parse_again = StateStack.state[ CTYPE_NEWLINE ]( 10 )
			end
		until parse_again ~= true
	end

	--[[ End of the Finite State Machine ]]--

	df.types = types
	df.classes = classes
	df.rules = rules
	df.data = data

	return df
end

function File:by_id( id )
	return self.data:by_id( id )
end

function File:get_type( id )
	for _, v in pairs( self.types ) do
		if ( v.id == id ) then
			return v
		end
	end

	return {}
end

function File:get_class( id, bubble )
	local c = self.classes[ id ]
	if c ~= nil then
		return c
	end

	if ( bubble == true ) then
		return self.rem_node:find_class( id )
	end

	return nil
end

function File:print()
	for _, v in pairs( self.types ) do
		v:print()
	end

	for k, v in pairs( self.classes:list()) do
		v:print()
	end

	for _, v in pairs( self.rules ) do
		v:print()
	end

	self.data:print_tree()
end

function File:post_load()
	self.data:post_load()
end