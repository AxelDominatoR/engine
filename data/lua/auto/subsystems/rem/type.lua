--[[
	Type class
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
Type = SimpleClass:inherit()

function Type:_init()
	SimpleClass._init( self )

	self._attributes = {}

	local mt = {}
	mt.__index = function( t, key )
		if ( Type[ key ] ~= nil ) then
			return Type[ key ]
		elseif ( t._attributes[ key ] ~= nil ) then
			return t._attributes[ key ]
		else
			print("REM type key " .. key .. " not found!")
		end
	end

	setmetatable( self, mt )
end

function Type:print()
	self:print_attributes( 0 )
end

-- optimizations
local stringsub = string.sub
local next = next
local tabs = utils.print_tabs

function Type:print_attributes( lvl )
	local data = {}
	local no_data = true
	for k, v in pairs( self._attributes ) do
		table.insert( data, k .. v:print())
		no_data = false
	end

	local info = {}
	table.insert( info, tabs( lvl ))
	if ( self.id ~= nil ) then table.insert( info, "type " .. self.id ) end
	if ( no_data == false ) then table.insert( info, "{" .. table.concat( data, ", ") .. "}") end

	print( table.concat( info, " "))
end