cache					= {}		-- prefab by alias

-- core.interface.import.prefabrem(alias,path,args)
getmetatable(this).__call = function(self,alias,path,rem,args)
	local data = { __info={}, __args=args or {}, __alias=alias, __scriptEnv=nil }

	-- Can either load *.xml directly or a folder that contains *.xml by the same name
	if (filesystem.isDirectory(path)) then
		local fname = path:gsub("(.*[\\/])",""):gsub("(%..*)","")
		local xml_path = path .. "/" .. fname .. ".xml"
		if (filesystem.exists(xml_path)) then
--			process_as_xml(xml_path,data)
		end
		process_as_rem( rem, data )
		local script_path = path .. "/" .. fname .. ".lua"
		process_script(script_path,data)
	else
		local ext = utils.get_ext(path)
		if (ext == "xml") then
--			process_as_xml(path,data)
		end
	end

	-- call initializer for optional script
	if (data.__scriptEnv and data.__scriptEnv.init) then
		data.__scriptEnv.init(data)
	end

	cache[alias] = data
	return data
end

function exists(alias)
	return cache[alias] ~= nil
end

function remove(alias)
	table.insert(core.interface.api._queuedForRemoval,alias)
end

function process_as_rem( rem, data )
	local depth = 0
	local api = core.interface.api
	local _stack = api._stack

	local cb_start = function( node )
		node = node:export_to_nk()
		if node._type ~= nil then
			local functor = api[ node._type ]
			if ( functor ) then
				table.insert( _stack, { functor, node, data, depth })
			end
			depth = depth + 1
		end

		print("CB_START: " .. node._type )
	end

	local cb_end = function( node )
		node = node:export_to_nk()
		if node._type ~= nil then
			depth = depth - 1
			node.__content = node.content or "" -- simplify params

			if node.title ~= nil then
				node.title = node.title.value
			end

			local functor = api[ node._type .. "_end"]
			if ( functor ) then
				table.insert( _stack, { functor, node, data, depth })
			end
		end

		print("CB_END: " .. node._type )
	end

	data.__stack_begin = #_stack
	rem.data:recurse( cb_start, cb_end )
	data.__stack_end = #_stack

	-- Load script if <script> tag exists
--	local node = xml.data.xml.script
--	if (node) then
--		process_script(node[1][1].name,data)
--	end
end

-------------------------------------------
-- Helpers
-------------------------------------------
function process_as_xml(path,data)
	local depth = 0
	local api = core.interface.api
	local _stack = api._stack

	local xml = core.framework.import.xml()
	xml.start_element_callback = function(_xml,node,parent,element)
		local functor = api[element]
		if (functor) then
			table.insert(_stack,{functor,node[1],data,depth})
		end
		depth = depth + 1
	end
	xml.end_element_callback = function(_xml,node,parent,element)
		depth = depth - 1
		node[1].__content = node[2] -- simplify params
		local functor = api[element.."_end"]
		if (functor) then
			table.insert(_stack,{functor,node[1],data,depth})
		end
	end

	data.__stack_begin = #_stack
	xml:parse(path)
	data.__stack_end = #_stack

	-- Load script if <script> tag exists
	local node = xml.data.xml.script
	if (node) then
		process_script(node[1][1].name,data)
	end
end

function process_script(fname,data)
	if (filesystem.exists(fname)) then
		local old_package_path = package.path -- Because we don't want anywhere else to accidentally require scripts in prefab directories
		package.path = package.path .. ";" .. utils.get_path(fname) .. "?.lua"

		data.__scriptEnv = core.interface.import.script(fname)

		package.path = old_package_path
	end
end