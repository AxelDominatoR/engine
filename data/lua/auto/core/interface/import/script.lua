-- It is up to caller to cache or not the script environment

-- core.interface.import.script(path)
getmetatable(this).__call = function(self,path)
	local chunk,err = filesystem.loadfile(path)
	if (chunk ~= nil) then
		local env = {}
		setmetatable(env,{__index=_G})
		setfenv(chunk,env)

		local status,result = xpcall(chunk,STP.stacktrace)
		
		if (status) then
			print("loaded interface script: " .. path)
		else
			print("[ERROR] failed to load script: " .. path .. "\n" .. result)
		end
		
		return env
	else
		printf(debug.traceback(1).."\n".."ERROR: "..err)
	end
end