cache					= {}		-- prefab by alias
local curID				= 0			-- Default ID of element if undefined in prefab

-- core.interface.import.prefab(alias,path,args)
getmetatable(this).__call = function(self,alias,path,args)
	local data = { __info={}, __args=args or {}, __alias=alias, __scriptEnv=nil }
	
	-- Can either load *.xml directly or a folder that contains *.xml by the same name
	if (filesystem.isDirectory(path)) then
		local fname = path:gsub("(.*[\\/])",""):gsub("(%..*)","")
		local xml_path = path .. "/" .. fname .. ".xml"
		if (filesystem.exists(xml_path) and not filesystem.isDirectory(xml_path)) then
			process_as_xml(xml_path,data)
		end
		local script_path = path .. "/" .. fname .. ".lua"
		process_script(script_path,data)
	else
		local ext = utils.get_ext(path)
		if (ext == "xml") then 
			process_as_xml(path,data)
		end
	end
	
	-- call initializer for optional script
	if (data.__scriptEnv and data.__scriptEnv.init) then
		data.__scriptEnv.init(data)
	end
		
	cache[alias] = data
	return data
end

function exists(alias)
	return cache[alias] ~= nil
end

function remove(alias)
	table.insert(core.interface.api._queuedForRemoval,alias)
end

-------------------------------------------
-- Helpers
-------------------------------------------
function process_as_xml(path,data)
	local depth = 0
	local api = core.interface.api
	local _stack = api._stack
	
	local xml = core.framework.import.xml()
	xml.start_element_callback = function(_xml,node,parent,element)
		local functor = api[element]
		if (functor) then
			table.insert(_stack,{functor,node[ 1 ],data,depth})
		end
		depth = depth + 1
	end
	xml.end_element_callback = function(_xml,node,parent,element)
		depth = depth - 1
		node[ 1 ].__content = node[ 2 ] -- simplify params
		if not (node[ 1 ].id) then
			node[ 1 ].id = "element" .. tostring(curID) -- If 'id' field is undefined assign an id
			curID = curID + 1
		end
		local functor = api[ element.."_end" ]
		if (functor) then
			table.insert(_stack,{functor,node[ 1 ],data,depth})
		end
	end
	
	data.__stack_begin = #_stack
	xml:parse(path)
	data.__stack_end = #_stack
	
	-- Load script if <script> tag exists
	local node = xml.data.xml.script
	if (node) then
		process_script(node[ 1 ][ 1 ].name,data)
	end
end

function process_script(fname,data)
	if (filesystem.exists(fname) and not filesystem.isDirectory(fname)) then
		local old_package_path = package.path -- Because we don't want anywhere else to accidentally require scripts in prefab directories
		package.path = package.path .. ";" .. utils.get_path(fname) .. "?.lua"
		
		data.__scriptEnv = core.interface.import.script(fname)
		
		package.path = old_package_path
	end
end