_font				= nil 			-- Current font

function setFont(alias)
	alias = alias or "default"
	local pFont = backend.cache[alias]
	if (pFont) then
		nuklear.nk_style_set_font(backend._context,pFont.handle)
		_font = alias
	end
end

function getFont()
	return _font and backend.cache[_font]
end