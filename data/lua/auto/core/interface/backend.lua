local nk = nuklear

_context 			= nil
max_vertex_buffer 	= 512 * 1024
max_element_buffer 	= 128 * 1024
local VAO = nil
local VBO = nil
local EBO = nil

cache 				= {} -- for fonts  *TODO* Figure out how to use the baked fonts for graphics.import.font

local gfx = core.framework.graphics

function on_game_start()
	CallbackSet("framework_load",on_load)
 	CallbackSet("framework_before_polling",input_begin)
	CallbackSet("framework_event_polling",handle_event)
	CallbackSet("framework_after_polling",input_end)
	CallbackSet("framework_draw_end",draw_end,-999)
	CallbackSet("framework_exit",on_exit)
end

function on_load()
	_context = nk.nk_gamma_init()
	
	-- Generate font stash
	nuklear.nk_gamma_font_stash_begin()
	--
 	local xml = core.framework.import.xml():parse("configs/ui/fonts.xml")
	if (xml.data.xml[ 1 ].font) then
		for i,element in ipairs(xml.data.xml[ 1 ].font) do
			local file = physfs.PHYSFS_openRead(element[ 1 ].filename)
			if (file ~= nil) then
				local length = physfs.PHYSFS_fileLength(file)
				local buffer = ffi.new("char[?]",length)
				physfs.PHYSFS_read(file,buffer,1,length)
				physfs.PHYSFS_close(file)

				local height = tonumber(element[ 1 ].height) or 12
				local pFont = nuklear.nk_gamma_add_font(buffer,length,height,nil)
				cache[ element[ 1 ].name ] = pFont
			end
		
		end
	end
	--
	nuklear.nk_gamma_font_stash_end()
	
	-- Set default font
	if (xml.data.xml[ 1 ].default) then
		local alias = xml.data.xml[ 1 ].default[ 1 ][ 1 ].name
		local default = cache[alias]
		assert(default,"ERROR: No default font set!")
		cache.default = default
		nuklear.nk_style_set_font(_context,default.handle)
		core.interface._font = "default"
	end
	
	VAO = ffi.new("GLuint[1]")
	VBO = ffi.new("GLuint[1]")
	EBO = ffi.new("GLuint[1]")
	
	GL.glGenVertexArrays(1,VAO)
	GL.glGenBuffers(1,VBO)
	GL.glGenBuffers(1,EBO)
	
	ffi.gc(VAO,function(buffer) GL.glDeleteVertexArrays(1, buffer) end)
	ffi.gc(VBO,function(buffer) GL.glDeleteBuffers(1, buffer) end)
	ffi.gc(EBO,function(buffer) GL.glDeleteBuffers(1, buffer) end)
	
	gfx.shaders.useProgram("nuklear")
	GL.glBindVertexArray(VAO[0])
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,VBO[0])
	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,EBO[0])
	
	GL.glBufferData(GL.GL_ARRAY_BUFFER,max_vertex_buffer,nil,GL.GL_STREAM_DRAW)
	GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER,max_element_buffer,nil,GL.GL_STREAM_DRAW)
	
	local tex = gfx.shaders.getUniformLocation("tex")
	GL.glUniform1i(tex,0)
	
	local position = gfx.shaders.getAttribLocation("position")
	local texcoord = gfx.shaders.getAttribLocation("texcoord")
	local color = gfx.shaders.getAttribLocation("color")

	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(texcoord)
	GL.glEnableVertexAttribArray(color)
	
	local stride = (4*ffi.sizeof("GLfloat"))+(4*ffi.sizeof("GLubyte"))
	local vt = ffi.cast("GLvoid *",(2)*ffi.sizeof("GLfloat"))
	local vc = ffi.cast("GLvoid *",(2+2)*ffi.sizeof("GLfloat"))
	
	GL.glVertexAttribPointer(position,2,GL.GL_FLOAT,0,stride,nil)
	GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vt)
	GL.glVertexAttribPointer(color,4,GL.GL_UNSIGNED_BYTE,1,stride,vc)
	
	GL.glBindVertexArray(0)
end

function input_begin()
	nk.nk_input_begin(_context)
end

function input_end()
	nk.nk_input_end(_context)
	
	local onUpdate = core.interface.api.onUpdate
	onUpdate(_context)
end

function handle_event(e)
	nk.nk_gamma_handle_event(e)
	
	if (nk.nk_item_is_any_active(_context) == 1) then
		core.framework.disableInput = true
	end
end

function draw_end()
	local w,h = core.framework.window.getSize()
	local dw,dh = gfx.getSize()
	local sx = dw/w
	local sy = dh/h
	
	gfx.transform.setOrthographicProjection()
	gfx.shaders.useProgram("nuklear")
	
	GL.glBindVertexArray(VAO[0])
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,VBO[0])
	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,EBO[0])

	local vertices = GL.glMapBuffer(GL.GL_ARRAY_BUFFER,GL.GL_WRITE_ONLY)
	local elements = GL.glMapBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,GL.GL_WRITE_ONLY)
	
	if (vertices ~= nil and elements ~= nil) then
		nk.nk_gamma_convert_buffers(nk.NK_ANTI_ALIASING_ON,vertices,elements,max_vertex_buffer,max_element_buffer)
	end
	
	GL.glUnmapBuffer(GL.GL_ARRAY_BUFFER)
	GL.glUnmapBuffer(GL.GL_ELEMENT_ARRAY_BUFFER)
	
	GL.glDisable(GL.GL_CULL_FACE)
	GL.glDisable(GL.GL_DEPTH_TEST)
	GL.glEnable(GL.GL_SCISSOR_TEST)

	gfx.transform.updateTransformations()
	nk.nk_gamma_draw(h,sx,sy)
	
	GL.glDisable(GL.GL_SCISSOR_TEST)
	GL.glEnable(GL.GL_CULL_FACE)
	GL.glEnable(GL.GL_DEPTH_TEST)
end

function on_exit()
	_context = nil
	nk.nk_gamma_device_destroy()
end