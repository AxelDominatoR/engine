local gfx = core.framework.graphics

-- core.entity.component.sprite()
getmetatable(this).__call = function (self,entity)
	return cSpriteManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cSpriteManager"
function cSpriteManager:__init(entity)
	self.dependencies = {"spatial"}
	self.entity = entity
	self.properties = { visible=true, animate=true, image=nil, layer=nil }
end

function cSpriteManager:image(path)
	if (path == nil) then
		return self.properties.image
	end
	if (self.properties.image and self.properties.image.name == path) then 
		return self.properties.image
	end
	local image_import = core.framework.graphics.import.image
	local image = path ~= "" and (image_import.animation.exists(path) and image_import.animation(path,true) or image_import.exists(path) and image_import(path))
	if (image) then
		self.properties.image = image
		if (image.reset) then
			image:reset()
		end
	else
		self.properties.image = nil
	end
	return self.properties.image
end

function cSpriteManager:update(dt,mx,my)
	if (self.properties.animate and self.properties.image and self.properties.image.image_animation_update) then
		self.properties.image:image_animation_update(dt,mx,my)
	end
end

function cSpriteManager:draw()
	if (self.properties.visible ~= true or self.properties.image == nil) then 
		return
	end
	
	gfx.transform.push()
		local pos = self.entity.component.spatial:position()
		local ang = self.entity.component.spatial:angle()
		local w,h = self:getTextureSize()
		
		gfx.transform.translate(pos[1],pos[2],pos[3])
 		gfx.transform.rotateZ(ang[3])
		
		self.properties.image:draw()
	gfx.transform.pop()
end

function cSpriteManager:getTexture()
	return self.properties.image ~= nil and self.properties.image:getTexture() or gfx.getDefaultTexture()
end

function cSpriteManager:getRegion()
	if (self.properties.image == nil) then 
		return 0,0,0,0
	end
	return self.properties.image:getRegion()
end

function cSpriteManager:getTextureSize()
	if (self.properties.image == nil) then 
		return 0,0
	end
	return self.properties.image:getTextureSize()
end

function cSpriteManager:stateWrite(data)
	data.path = self.properties.image and self.properties.image.name or nil
end 

function cSpriteManager:stateRead(data)
	self:image(data.path)
end

-------------------------------------
-- Editor controls
-------------------------------------
function editor_controls(entity,component,ctx,node,data,depth)
	local p = data.tree_components
	if not (p.__node) then
		p.__node = {}
	end
	
	-- Visible
	component.properties.visible = nuklear.nk_check_label(ctx,"Visible",not component.properties.visible) == 0
	
	nuklear.nk_layout_row_dynamic(ctx,28,2)
	
	-- Image
	if not (p.__node.image_path_label) then
		p.__node.image_path_label = {id="editor_controls_sprite__image_path_label",flags="NK_TEXT_ALIGN_LEFT",__content="st_image"}
	end
	core.interface.api.label(ctx,p.__node.image_path_label,data,depth)
	if not (p.__node.image_path_edit) then
		p.__node.image_path_edit = {id="editor_controls_sprite__image_path_edit",max=1024,filter="nk_filter_default",flags="NK_EDIT_BOX",__content=(component.properties.image and component.properties.image.name or "")}
	end
	if (core.interface.api.edit_string(ctx,p.__node.image_path_edit,data,depth) ~= nil) then
		local image_path = ffi.string(data[ p.__node.image_path_edit.id ].text)
		component:image(image_path)
	end
	
	if (component.properties.image and component.properties.image.frame) then
		component.properties.image.state = nuklear.nk_check_label(ctx,"Animate",component.properties.image.state == 0) == 0 and 1 or 0
		nuklear.nk_label(ctx,"Index:"..component.properties.image.index,nuklear.NK_TEXT_ALIGN_LEFT)
		p.__node.image_index = ffi.new("float[1]",component.properties.image.index)
		if (nuklear.nk_slider_float(ctx,1,p.__node.image_index,component.properties.image.size,1) == 1) then
			component.properties.image.index = p.__node.image_index[0]
		end
		component.properties.image.timeFactor = nuklear.nk_propertyf(ctx, "#Speed:", 0, component.properties.image.timeFactor, 5, 0.1,0.1)
	end
end

function entity_editor_submit(entity,component,ctx,node,data,depth)
	local p = data.tree_components
	local path = p and p.__node and p.__node.image_path_edit and ffi.string(data[ p.__node.image_path_edit.id ].text)
	if (path and path ~= "") then
		component:image(path)
	end
end