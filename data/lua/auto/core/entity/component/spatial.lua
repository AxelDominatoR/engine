
-- core.entity.component.spatial()
getmetatable(this).__call = function (self,entity)
	return cSpatialManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cSpatialManager"
function cSpatialManager:__init(entity)
	self.entity = entity
	self.properties = { position={0,0,0}, angle={0,0,0}, orientation=ffi.new("kmQuaternion",0,0,0,1) }
end

function cSpatialManager:position(x,y,z)
	local pos = self.properties.position
	if (x or y or z) then 
		pos[1] = x or pos[1]
		pos[2] = y or pos[2]
		pos[3] = z or pos[3]
	end
	return pos
end

function cSpatialManager:angle(p,y,r)
	local ang = self.properties.angle
	if (p or y or r) then
		ang[1] = utils.wrap_minmax(p or ang[1],-2*math.pi,2*math.pi)
		ang[2] = utils.wrap_minmax(y or ang[2],-2*math.pi,2*math.pi)
		ang[3] = utils.wrap_minmax(r or ang[3],-2*math.pi,2*math.pi)
		kazmath.kmQuaternionRotationPitchYawRoll(self.properties.orientation,unpack(ang))
	end
	return ang
end

function cSpatialManager:orientation(p,y,r)
	self:angle(p,y,r)
	return self.properties.orientation
end

-------------------------------
-- persistance
-------------------------------
function cSpatialManager:stateWrite(data)
	local p = self.properties.position
	data.position = {p[1],p[2],p[3]}
	p = self.properties.angle
	data.angle = {p[1],p[2],p[3]}
end

function cSpatialManager:stateRead(data)
	self.properties.position = data.position or self.properties.position
	self.properties.angle = data.angle or self.properties.angle
	kazmath.kmQuaternionRotationPitchYawRoll(self.properties.orientation,unpack(self.properties.angle))
end

--------------------------------
-- Editor controls
--------------------------------
function editor_controls(entity,component,ctx,node,data,depth)
	local size = ffi.new("struct nk_vec2")
	size.x = 300 -- w
	size.y = 200 -- h
	if (nuklear.nk_combo_begin_label(ctx,"Position",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local x,y,z = unpack(component:position())
		x = nuklear.nk_propertyf(ctx, "#X:", -99999, x, 99999, 0.1,1)
		y = nuklear.nk_propertyf(ctx, "#Y:", -99999, y, 99999, 0.1,1)
		z = nuklear.nk_propertyf(ctx, "#Z:", -99999, z, 99999, 0.1,1)
		component:position(x,y,z)
		nuklear.nk_combo_end(ctx)
	end
	if (nuklear.nk_combo_begin_label(ctx,"Angle",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local x,y,z = unpack(component:angle())
		x = utils.wrap_minmax(nuklear.nk_propertyf(ctx, "#Pitch:", -361, x*180/math.pi, 361, 1,1)*math.pi/180,-2*math.pi,2*math.pi)
		y = utils.wrap_minmax(nuklear.nk_propertyf(ctx, "#Yaw:", -361, y*180/math.pi, 361, 1,1)*math.pi/180,-2*math.pi,2*math.pi)
		z = utils.wrap_minmax(nuklear.nk_propertyf(ctx, "#Roll:", -361, z*180/math.pi, 361, 1,1)*math.pi/180,-2*math.pi,2*math.pi)
		component:angle(x,y,z)
		nuklear.nk_combo_end(ctx)
	end
end