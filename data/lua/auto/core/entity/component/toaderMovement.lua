-- core.entity.component.toaderMovement()
getmetatable(this).__call = function (self,entity)
	local t = {
		dependencies			= {"sprite"},
		active					= true,
		entity					= entity,
		update					= update,
		key_pressed 			= key_pressed
	}
	return t
end

local hopTime = 0
local hopDir = 0

function update(self,dt,mx,my)
	if not (self.active) then
		return
	end
	if (time.time() > hopTime) then
		if (hopDir == 0) then
			self.entity.component.sprite:image("toader_idle_front")
		else
			self.entity.component.sprite:image("toader_idle_back")
		end
	end
end

function key_pressed(self,key,scancode,aliases,isrepeat)
	if not (self.active) then
		return
	end
	
	if not (aliases) then
		return
	end
	
	local x,y
	if (aliases.up) then
		y = -64
		self.entity.component.sprite:image("toader_hop_front")
		hopTime = time.time() + 0.20
		hopDir = 0
	end
	if (aliases.down) then
		y = 64
		self.entity.component.sprite:image("toader_hop_back")
		hopDir = 1
		hopTime = time.time() + 0.20
	end
	if (aliases.left) then
		x = -64
		if (hopDir == 0) then 
			self.entity.component.sprite:image("toader_hop_front")
		else 
			self.entity.component.sprite:image("toader_hop_back")
		end
		hopTime = time.time() + 0.15
	end
	if (aliases.right) then
		x = 64
		if (hopDir == 0) then 
			self.entity.component.sprite:image("toader_hop_front")
		else 
			self.entity.component.sprite:image("toader_hop_back")
		end
		hopTime = time.time() + 0.15
	end
	
	if (x or y) then
		local pos = self.entity.component.spatial:position()
		pos[1] = pos[1] + (x or 0)
		pos[2] = pos[2] + (y or 0)
	end
end

-------------------------------------
-- Editor controls
-------------------------------------
function editor_controls(entity,component,ctx,node,data,depth)
	component.active = nuklear.nk_check_label(ctx,"Active",not component.active) == 0
end

function entity_editor_submit(entity,component,ctx,node,data,depth)

end