-- TODO: replace 'eng' with language type from config

cache = {}

-- core.translate(str)
getmetatable(this).__call = function (self,str)
	-- XXX consider utils.trim but needless overhead?
	return str and cache[str] or str
end

function on_game_start()
	-- Autoload all translatable strings
	local function on_execute(path,fname,fullpath)
		local xml = core.framework.import.xml():parse(fullpath)
		for i,element in ipairs(xml.data.xml[ 1 ].string) do
			if (element[1].id) then
				cache[element[1].id] = element[2] -- TODO: Add tokenism and possibly allow assert(loadstring())()  something like "$lua func_that_returns_str()$"
			end
		end
	end
	filesystem.fileForEach("configs/translation/eng",true,{"xml"},on_execute)
end