SimpleClass = {}
function SimpleClass:new( o )
	o = o or {}

	self.__index = self
	setmetatable( o, self )

	o:_init()

	return o
end

function SimpleClass:inherit( o )
	o = o or {}

	self.__index = self
	setmetatable( o, self )

	return o
end

function SimpleClass:_init()
end