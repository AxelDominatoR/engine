--[[
	Very simple tree node class
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
Node = SimpleClass:inherit()

function Node:_init()
	SimpleClass._init( self )
	self._children = {}
end

function Node:by_id( id )
	for _, v in pairs( self._children ) do
		if ( v.id == id ) then
			return v
		end
	end
end

function Node:add_child( child, name )
	child._parent = self

	if name ~= nil then
		self._children[ name ] = child
	else
		table.insert( self._children, child )
	end
end

function Node:has_child( name )
	return self._children[ name ] ~= nil
end

function Node:get_child( name )
	return self._children[ name ]
end

function Node:print_tree( lvl )
	lvl = lvl or 0

	for _, v in pairs( self._children ) do
		v:print_attributes( lvl )
		v:print_tree( lvl + 1 )
	end
end

function Node:post_load()
	print("BASE NODE POST LOAD")
end

-- optimizations
local stringsub = string.sub
local next = next
local tabs = utils.print_tabs

function Node:print_attributes( lvl )
	local data = {}
	local no_data = true
	for k, v in pairs( self ) do
		if ( stringsub( k, 1, 1 ) ~= "_") and ( k ~= "id") then
			if ( v.value ~= nil ) then
				table.insert( data, k .. v.op .. "\"" .. v.value .. "\"")
			else
				table.insert( data, k .. "=\"" .. v .. "\"")
			end

			no_data = false
		end
	end

	local info = {}
	table.insert( info, tabs( lvl ))
	if ( self._type ~= nil ) then table.insert( info, self._type ) end
	if ( self.id ~= nil ) then table.insert( info, "#" .. self.id ) end
	if ( no_data == false ) then table.insert( info, "{" .. table.concat( data, ", ") .. "}") end

	print( table.concat( info, " "))
end

function Node:recurse( cb_start, cb_end )
	if cb_start ~= nil then
		cb_start( self )
	end

	for _, v in pairs( self._children ) do
		v:recurse( cb_start, cb_end )
	end

	if cb_end ~= nil then
		cb_end( self )
	end
end