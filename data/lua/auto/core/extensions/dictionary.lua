--[[
	Dictionary
	Index-based array with added functionality
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
Dictionary = SimpleClass:inherit()

function Dictionary:_init()
	SimpleClass._init( self )

	self.key = self.key or "id"   -- use "id" by default
	self.items = {}

	local mt = {}
	mt.__index = function( t, key )
		if ( Dictionary[ key ] ~= nil ) then
			return Dictionary[ key ]
		elseif ( t.items[ key ] ~= nil ) then
			return t.items[ key ]
		else
			print("Dictionary key " .. key .. " not found!")
		end
	end

	setmetatable( self, mt )
end

function Dictionary:add( item )
	local key = item[ self.key ]
	if key ~= nil then
		self.items[ key ] = item
	else
		printf("Key %s not found in item", self.key )
	end
end

function Dictionary:list()
	return self.items
end

function Dictionary:to_string()
	local res = {}
	for k, v in pairs( self.items ) do
		table.insert( res, v:to_string())
	end

	return table.concat( res, ", ")
end