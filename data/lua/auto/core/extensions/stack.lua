--[[
	Stack class
--]]

local SimpleClass = core.extensions.simple_class.SimpleClass
Stack = SimpleClass:inherit()

function Stack:_init()
	SimpleClass._init( self )
	self.stack = {}
end

function Stack:push( item )
	table.insert( self.stack, item )
end

function Stack:pop()
	return table.remove( self.stack )
end

function Stack:peek()
	return self.stack[ #self.stack ]
end