local x,y = ffi.new("int[1]"), ffi.new("int[1]")

function position()
	SDL.SDL_GetMouseState(x,y)
	return x[0], y[0]
end

function pressed(button)
	return bit.band(SDL.SDL_GetMouseState(nil,nil), bit.lshift(1,button-1)) ~= 0
end 

function showCursor(enable)
	SDL.SDL_ShowCursor(enable and SDL.SDL_ENABLE or SDL.SDL_DISABLE)
end

function setCursor(cursor)
 	if (cursor) then
		SDL.SDL_SetCursor(cursor)
 	else
 		SDL.SDL_SetCursor(SDL.SDL_GetDefaultCursor())
 	end
end
	
function setPosition(x,y)
	SDL.SDL_WarpMouseInWindow(core.framework.window._window,x,y)
end

function setVisible(visible)	 
	SDL.SDL_ShowCursor(visible and 1 or 0)
end

function isVisible()
	return SDL.SDL_ShowCursor(-1) == 1
end