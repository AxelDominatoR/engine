_window = nil
_context = nil

function on_game_start()
	local section = "window"
	core.config.set_default(section,"opengl_version","3.3")
	core.config.set_default(section,"title","")
	core.config.set_default(section,"width",1280)
	core.config.set_default(section,"height",800)
	core.config.set_default(section,"flags",bit.bor(ffi.C.SDL_WINDOW_OPENGL,ffi.C.SDL_WINDOW_SHOWN))
	core.config.set_default(section,"msaa",0)
end

function create()
	local config = core.config.data.window
	
	local x = tonumber(config.x) or SDL.SDL_WINDOWPOS_UNDEFINED
	local y = tonumber(config.y) or SDL.SDL_WINDOWPOS_UNDEFINED
	local width = tonumber(config.width) or 800
	local height = tonumber(config.height) or 600
	local flags = tonumber(config.flags) or 0

	local defaultFlags = bit.bor(ffi.C.SDL_WINDOW_OPENGL,ffi.C.SDL_WINDOW_SHOWN)
	flags = bit.bor(defaultFlags, flags or 0)

	SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING)
	
	-- OpenGL context version
	local version = utils.str_explode(core.config.r_value("window","opengl_version"),".",true)
	
	SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_FLAGS,ffi.C.SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG)
	SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_PROFILE_MASK,ffi.C.SDL_GL_CONTEXT_PROFILE_CORE)
	SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_MAJOR_VERSION, tonumber(version[1]))
	SDL.SDL_GL_SetAttribute(ffi.C.SDL_GL_CONTEXT_MINOR_VERSION, tonumber(version[2]))
	
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_RED_SIZE,8)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_GREEN_SIZE,8)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_BLUE_SIZE,8)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_ALPHA_SIZE,8)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_BUFFER_SIZE,32)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_DEPTH_SIZE,24)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_DOUBLEBUFFER,1)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_ACCELERATED_VISUAL,1)
	
	local msaa = tonumber(config.msaa) or 0
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_MULTISAMPLEBUFFERS,msaa == 0 and 0 or 1)
	SDL.SDL_GL_SetAttribute(SDL.SDL_GL_MULTISAMPLESAMPLES,msaa)
	
	_window  = SDL.SDL_CreateWindow(config.title or "",x,y,width,height,flags)
	if (_window == nil) then
		local err = SDL.SDL_GetError()
		error( "SDL_CreateWindow failed: " .. (err == nil and "unknown" or ffi.string(err)) )
	end
	
	_context = SDL.SDL_GL_CreateContext(_window)
	if (_context == nil) then
		local err = SDL.SDL_GetError()
		error( "SDL_GL_CreateContext failed: " .. (err == nil and "unknown" or ffi.string(err)) )
	end

	local width, height = core.framework.graphics.getSize()
	core.framework.graphics.setViewport(0,0,width,height)
	
	GL.glBlendEquation(GL.GL_FUNC_ADD)
	GL.glEnable(GL.GL_BLEND)
	GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
	GL.glClearDepth(1)
	GL.glEnable(GL.GL_DEPTH_TEST)
	GL.glEnable(GL.GL_CULL_FACE)
	GL.glCullFace(GL.GL_BACK)
	GL.glDepthFunc(GL.GL_LEQUAL)
	GL.glEnable(GL.GL_TEXTURE_CUBE_MAP_SEAMLESS)

	if (msaa == 0) then 
		GL.glDisable(GL.GL_MULTISAMPLE)
	else
		GL.glEnable(GL.GL_MULTISAMPLE)
	end

	print("OpenGL version: "..ffi.string(GL.glGetString(GL.GL_VERSION)))
	--print("OpenGL extensions: "..)
--[[ 	_G.GL_EXTENSIONS = {}
	ffi.string(GL.glGetString(GL.GL_EXTENSIONS)):gsub("[^%s]*",function(c) _G.GL_EXTENSIONS[c] = true end)
	print("ARB_base_instance",_G.GL_EXTENSIONS["GL_ARB_base_instance"]) ]]
end

function getPixelScale()
	local gw = core.framework.graphics.getSize()
	local ww = getSize()
	return gw / ww
end

function getSize()
	local width  = ffi.new("int[1]")
	local height = ffi.new("int[1]")
	SDL.SDL_GetWindowSize(_window, width, height)
	return width[0], height[0]
end

function resize(width,height)
	SDL.SDL_SetWindowSize(_window, width, height)
	core.framework.graphics.setViewport(0,0,width,height)
end

function swap()
	SDL.SDL_GL_SwapWindow(_window)
	core.framework.graphics._drawCalls = 0
end

function setTitle(title)
	SDL.SDL_SetWindowTitle(_window,tostring(title))
end 

function getTitle(title)
	return ffi.string(SDL.SDL_GetWindowTitle())
end

function getDisplayModes(index)
	index = index or 0
	local count = SDL.SDL_GetNumDisplayModes(index)
	local mode = ffi.new("SDL_DisplayMode")
	local t = {}
	for i=0,count-1 do 
		if (SDL.SDL_GetDisplayMode(index,i,mode) == 0) then 
			--print(mode.w,mode.h,mode.format,mode.refresh_rate)
			table.insert(t,{mode.w,mode.h,mode.format,mode.refresh_rate})
		else
			print("Failed to get display modes")
		end
	end
	return t
end