aliases = nil 
keyToAlias = {}

function on_game_start()
	core.config.set_default("keybinding","keyboard",{
		["pause"] 		= {"Escape",false},
		["action"] 		= {"E",true},
		["up"] 			= {"W",true},
		["left"]		= {"A",true},
		["down"] 		= {"S",true},
		["right"] 		= {"D",true}
	})
	aliases = core.config.data.keybinding.keyboard
	for alias,t in pairs(aliases) do 
		if not (keyToAlias[t[1]]) then 
			keyToAlias[t[1]] = {}
		end
		keyToAlias[t[1]][alias] = true
	end
end

function pressed(key)
	local state = SDL.SDL_GetKeyboardState(nil)
	key = SDL.SDL_GetKeyFromName(key) -- https://wiki.libsdl.org/SDL_Keycode
	key = SDL.SDL_GetScancodeFromKey(key)
	return state[key] == 1 or false
end

function scancodePressed(key)
	local state = SDL.SDL_GetKeyboardState(nil)
	key = SDL.SDL_GetScancodeFromName(key)
	return state[key] == 1 or false
end

function aliasPressed(alias)
	local key = keyboard_aliases[alias] and keyboard_aliases[alias][1]
	return key and core.framework.keyboard.pressed(key) or false
end