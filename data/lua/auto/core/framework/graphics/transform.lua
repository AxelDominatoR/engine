_mode  				= "model"
_stack 				= {}
_transformations 	= {"model","view","projection"}

for _, transformation in ipairs(_transformations) do
	local mat4 = ffi.new("kmMat4")
	kazmath.kmMat4Identity(mat4)
	_stack[transformation] = {mat4}
end

function push()
	local top = ffi.new("kmMat4")
	local mat4 = getTransformation()
	kazmath.kmMat4Assign(top,mat4)
	table.insert(_stack[_mode],top)
end

function pop()
	local stack = _stack[_mode]
	if (#stack == 1) then return end
	table.remove(stack, #stack)
end

function getTransformation(mode)
	local stack = _stack[mode or _mode]
	return stack[#stack]
end

function setPerspectiveProjection(fov,aspect,near,far)
	local mat4 = getTransformation("projection")
	kazmath.kmMat4PerspectiveProjection(mat4,fov,aspect,near,far)
end

function setReversedZPerspectiveProjection(fov,aspect,near)
	local mat4 = getTransformation("projection")
	kazmath.kmMat4ReversedZPerspectiveProjection( mat4, fov, aspect, near )
	GL.glDepthFunc(GL.GL_GEQUAL)
end

function setOrthographicProjection(width, height)
	if (not width and not height) then
		width, height = core.framework.graphics.getSize()
	end
	local mat4 = getTransformation("projection")
	kazmath.kmMat4OrthographicProjection(mat4, 0, width, height, 0, -1.0, 1.0)
end

function origin()
	local mat4 = getTransformation()
	kazmath.kmMat4Identity(mat4)
end

function lookAt( eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ )
	local mat4   = getTransformation("view")
	local eye    = ffi.new( "kmVec3", eyeX,    eyeY,    eyeZ )
	local center = ffi.new( "kmVec3", centerX, centerY, centerZ )
	local up     = ffi.new( "kmVec3", upX,     upY,     upZ )
	kazmath.kmMat4LookAt( mat4, eye, center, up )
end

function rotateX(rad)
	local rotation = ffi.new("kmMat4")
	kazmath.kmMat4RotationX(rotation, rad)
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, rotation)
end

function rotateY(rad)
	local rotation = ffi.new("kmMat4")
	kazmath.kmMat4RotationY(rotation, rad)
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, rotation)
end

function rotateZ(rad)
	local rotation = ffi.new("kmMat4")
	kazmath.kmMat4RotationZ(rotation, rad)
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, rotation)
end

function rotateYawPitchRoll(y,p,r)
	local rotation = ffi.new("kmMat4")
	kazmath.kmMat4RotationYawPitchRoll(rotation,y,p,r) -- pitch, yaw, roll as per docs
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, rotation)
end

function rotate(quat)
	local rotation = ffi.new("kmMat4")
	kazmath.kmMat4RotationQuaternion(rotation, quat)
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, rotation)
end

function getMatrixMode()
	return _mode
end

function setMatrixMode(mode)
	_mode = mode
end

function scale(x,y,z)
	z = z or 1
	local scaling = ffi.new("kmMat4")
	kazmath.kmMat4Scaling(scaling, x, y, z)
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, scaling)
end

function translate(x,y,z)
	z = z or 0
	local translation = ffi.new("kmMat4")
	kazmath.kmMat4Translation(translation, x, y, z)
	local mat4 = getTransformation()
	kazmath.kmMat4Multiply(mat4, mat4, translation)
end

function updateTransformations()
	for _, mode in ipairs(_transformations) do
		local uniform = core.framework.graphics.shaders.getUniformLocation(mode)
		local mat4 = getTransformation(mode)
		GL.glUniformMatrix4fv(uniform, 1, GL.GL_FALSE, mat4.mat)
	end
end

function unproject(z)
	local mx,my = core.framework.mouse.position()
	local vx,vy,vw,vh = core.framework.graphics.getViewport()
	local x = ((mx - vx) / vw) * 2 - 1
	local y = ((vh - my - vy) / vh) * 2 - 1
	
	local inverse = ffi.new("kmMat4")
	local projview = ffi.new("kmMat4")
	local proj = core.framework.graphics.transform.getTransformation("projection")
	local view = core.framework.graphics.transform.getTransformation("view")
	kazmath.kmMat4Multiply(projview,proj,view)
	kazmath.kmMat4Inverse(inverse,projview)
	
	local out = ffi.new("kmVec4")
	local pos = ffi.new("kmVec4",x,y,z,1)
	kazmath.kmVec4MultiplyMat4(out,pos,inverse)
	
	return out.x,out.y,out.z
end