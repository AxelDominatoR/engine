_shader = nil

cache = {}				-- Shader gluint by alias
shaderToAlias = {}		-- Find alias of a shader by it's value
script = {} 			-- Cache for optional shader script environments

local function getShaderCompileStatus(shader)
	local status = ffi.new("GLint[1]")
	GL.glGetShaderiv(shader, GL.GL_COMPILE_STATUS, status)
	if (status[0] ~= GL.GL_FALSE) then
		return
	end

	local length = ffi.new("GLint[1]")
	GL.glGetShaderiv(shader, GL.GL_INFO_LOG_LENGTH, length)
	if (length[0] > 0) then
		local buffer = ffi.new("char[?]", length[0])
		GL.glGetShaderInfoLog(shader, length[0], nil, buffer)
		GL.glDeleteShader(shader)
		error(ffi.string(buffer, length[0]),4)
	else
		GL.glDeleteShader(shader)
		error("Failed to compile shader",4)
	end
end

local function createShader(type, source)
	local shader = GL.glCreateShader(type)
	source = ffi.new("char[?]", #source, source)
	local str = ffi.new("const char *[1]", source)
	GL.glShaderSource(shader, 1, str, nil)
	GL.glCompileShader(shader)

	getShaderCompileStatus(shader)
	return shader
end

function on_game_start()
	CallbackSet("framework_load",on_load,999) -- high priority
end

function on_load()
	-- autoload shaders
	
	local xml = core.framework.import.xml():parse("configs/shaders.xml")
	for i,element in ipairs(xml.data.xml[ 1 ].shader) do
		local alias = element[ 1 ].name
		if (cache[ alias ]) then 
			print("shader redifinition!",element[ 1 ].name)
			return
		end
		
		local fragmentSource = filesystem.read(element[ 1 ].fragment)
		local vertexSource = filesystem.read(element[ 1 ].vertex)
		local fragmentShader = createShader(GL.GL_FRAGMENT_SHADER, fragmentSource)
		local vertexShader = createShader(GL.GL_VERTEX_SHADER, vertexSource)
		local shader = GL.glCreateProgram()
		shaderToAlias[ shader ] = alias
		GL.glAttachShader(shader, vertexShader)
		GL.glAttachShader(shader, fragmentShader)
		
		cache[ alias ] = shader
		-- Shaders can have optional script
		if (element[ 1 ].script) then
			local chunk,err = filesystem.loadfile(element[ 1 ].script)
			if (chunk ~= nil) then
				local env = {}
			
				setmetatable(env,{__index=function(t,k)
					return rawget(_G,k) -- ability to access _G
				end})
				
				setfenv(chunk,env)
				
				local status,err = xpcall(chunk,STP.stacktrace)
				if (status) then
					print("loaded shader script: " .. element[ 1 ].script)
					env._shader = shader
					env.init(shader)
					script[ alias ] = env
				else
					print("[ERROR] failed to load script: " .. element[ 1 ].script .. "\n" .. err)
				end
			else
				printf(debug.traceback(1).."\n".."ERROR: "..err)
			end
		end
	end
	
	useProgram("default2D")
end

function getActive()
	return _shader
end

function setActive(shader)
	if (_shader ~= shader) then
		GL.glUseProgram(shader)
		_shader = shader
	end
end

function useProgram(alias)
	local shader = cache[ alias ]
	if not (shader) then 
		print("setActive shader doesn't exist!",alias)
		return 
	end
	setActive(shader)
end

function getProgram(alias)
	return cache[ alias ]
end

-- https://forums.khronos.org/showthread.php/77574-Speed-of-glGetUniformLocation
local uniformCache = {}
local attribCache = {}

function getUniformLocation(name)
	local alias = shaderToAlias[ _shader ]
	if not (uniformCache[ alias ]) then
		uniformCache[ alias ] = {}
	end
	if not (uniformCache[ alias ][ name ]) then
		uniformCache[ alias ][ name ] = GL.glGetUniformLocation(_shader,name)
	end
	return uniformCache[ alias ][ name ]
end

function getAttribLocation(name)
	local alias = shaderToAlias[ _shader ]
	if not (attribCache[ alias ]) then
		attribCache[ alias ] = {}
	end
	if not (attribCache[ alias ][ name ]) then
		attribCache[ alias ][ name ] = GL.glGetAttribLocation(_shader,name)
	end
	return attribCache[ alias ][ name ]
end