local gfx = core.framework.graphics

VERTEX_BUFFER_ALLOC = 1000 * 2^10
INDEX_BUFFER_ALLOC 	= 1000 * 2^10
MATRIX_BUFFER_ALLOC	= 25 * 2^10
INDIRECT_BUFFER_ALLOC = 10 * 2^10

renderable_objects = {}

local bufferGroups = {}
local bufferObject = nil
local bufferNumber = 0
local bufferFences = {}
local bufferCount = 3

local need_recalculation = true

function on_game_start()
	--CallbackSet("framework_load",on_load)
	--CallbackSet("framework_draw_end",on_draw_end,-998)
end

function on_load()
	for i=1,bufferCount do
		bufferGroups[ i ] = cBufferGroup()
	end
	bufferObject = bufferGroups[ 1 ]
end

function on_draw_end(dt)
	local camera = core.camera.getActive3D()
	if not (camera) then
		return
	end
	
	gfx.shaders.useProgram("default3DInstanced")
	camera:push()
		
		bufferNumber = (bufferNumber + 1) % 3
		if (bufferNumber >= bufferCount) then
			bufferNumber = 0
		end
		
		local fence = bufferFences[ bufferNumber+1 ]
		if (fence) then
			local result = GL.glClientWaitSync(fence,0,0)
				
			if (result == GL.GL_WAIT_FAILED) then
				print("GL_WAIT_FAILED")
			end
			
			GL.glDeleteSync(fence)
		end
		
		bufferObject = bufferGroups[ bufferNumber+1 ]
		
		GL.glBindVertexArray(bufferObject.vao[0])
		GL.glBindBuffer(GL.GL_DRAW_INDIRECT_BUFFER,bufferObject.ibo[0])
		
		gfx.transform.updateTransformations()
		GL.glMultiDrawElementsIndirect(GL.GL_TRIANGLES,GL.GL_UNSIGNED_INT,nil,bufferObject.renderableCount,0)
		
		bufferFences[ bufferNumber+1 ] = GL.glFenceSync(GL.GL_SYNC_GPU_COMMANDS_COMPLETE ,0)
		
		if (need_recalculation) then
			for i=1,bufferCount do
				bufferGroups[ i ]:recalculate()
			end
			need_recalculation = false
		end
		
		GL.glBindVertexArray(0)
		GL.glBindBuffer(GL.GL_ARRAY_BUFFER,0)
		GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,0)
		GL.glBindBuffer(GL.GL_DRAW_INDIRECT_BUFFER,0)
	
	camera:pop()
end

function register(renderable)
	table.insert(renderable_objects,renderable)
	need_recalculation = true
end

Class "cBufferGroup"
function cBufferGroup:__init()
	self.vertex_buffer_alloc = VERTEX_BUFFER_ALLOC
	self.index_buffer_alloc = INDEX_BUFFER_ALLOC
	self.matrix_buffer_alloc = MATRIX_BUFFER_ALLOC
	self.indirect_buffer_alloc = INDIRECT_BUFFER_ALLOC
	
	self.renderableCount = 0

	self.vao = ffi.new("GLuint[1]")
	self.vbo = ffi.new("GLuint[1]")
	self.ebo = ffi.new("GLuint[1]")
	self.mbo = ffi.new("GLuint[1]")
	self.ibo = ffi.new("GLuint[1]")

	GL.glGenVertexArrays(1,self.vao)
	GL.glGenBuffers(1,self.vbo)
	GL.glGenBuffers(1,self.ebo)
	GL.glGenBuffers(1,self.mbo)
	GL.glGenBuffers(1,self.ibo)
	
	ffi.gc(self.vao,function(buffer) GL.glDeleteVertexArrays(1, buffer) end)
	ffi.gc(self.vbo,function(buffer) GL.glDeleteBuffers(1, buffer) end)
	ffi.gc(self.ebo,function(buffer) GL.glDeleteBuffers(1, buffer) end)
	ffi.gc(self.mbo,function(buffer) GL.glDeleteBuffers(1, buffer) end)
	ffi.gc(self.ibo,function(buffer) GL.glDeleteBuffers(1, buffer) end)
	
	gfx.shaders.useProgram("default3DInstanced")
	GL.glBindVertexArray(self.vao[0])

	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,self.vbo[0])
	GL.glBufferData(GL.GL_ARRAY_BUFFER,self.vertex_buffer_alloc,nil,GL.GL_STREAM_DRAW)

	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,self.ebo[0])
	GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER,self.index_buffer_alloc,nil,GL.GL_STREAM_DRAW)

	GL.glBindBuffer(GL.GL_DRAW_INDIRECT_BUFFER,self.ibo[0])
	GL.glBufferData(GL.GL_DRAW_INDIRECT_BUFFER,self.indirect_buffer_alloc,nil,GL.GL_STREAM_DRAW)

	local tex = gfx.shaders.getUniformLocation("tex")
	GL.glUniform1i(tex,3)
	
	local position = gfx.shaders.getAttribLocation("position")
	local normal = gfx.shaders.getAttribLocation("normal")
	local tangent = gfx.shaders.getAttribLocation("tangent")
	local bitangent = 3--gfx.shaders.getAttribLocation("bitangent")
	local texcoord = gfx.shaders.getAttribLocation("texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(normal)
	GL.glEnableVertexAttribArray(tangent)
	GL.glEnableVertexAttribArray(bitangent)
	GL.glEnableVertexAttribArray(texcoord)
	
	local stride = (3+3+3+3+2)*ffi.sizeof("GLfloat")
	local vno = ffi.cast("GLvoid *",3*ffi.sizeof("GLfloat"))  -- hold in local variable because of GC
	local vta = ffi.cast("GLvoid *",(3+3)*ffi.sizeof("GLfloat"))
	local vbi = ffi.cast("GLvoid *",(3+3+3)*ffi.sizeof("GLfloat"))
	local vte = ffi.cast("GLvoid *",(3+3+3+3)*ffi.sizeof("GLfloat"))
	
	GL.glVertexAttribPointer(position,3,GL.GL_FLOAT,0,stride,nil)
	GL.glVertexAttribPointer(normal,3,GL.GL_FLOAT,0,stride,vno)
	GL.glVertexAttribPointer(tangent,3,GL.GL_FLOAT,0,stride,vta)
	GL.glVertexAttribPointer(bitangent,3,GL.GL_FLOAT,0,stride,vbi)
	GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vte)
	
 	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,self.mbo[0])
	
	GL.glBufferData(GL.GL_ARRAY_BUFFER,self.matrix_buffer_alloc,nil,GL.GL_STREAM_DRAW)
	
	local modelMatrix = gfx.shaders.getAttribLocation("modelMatrix")
	
	GL.glEnableVertexAttribArray(modelMatrix+0)
	GL.glEnableVertexAttribArray(modelMatrix+1)
	GL.glEnableVertexAttribArray(modelMatrix+2)
	GL.glEnableVertexAttribArray(modelMatrix+3)

	GL.glVertexAttribPointer(modelMatrix+0,4,GL.GL_FLOAT,0,ffi.sizeof("kmMat4"),nil)
	GL.glVertexAttribPointer(modelMatrix+1,4,GL.GL_FLOAT,0,ffi.sizeof("kmMat4"),ffi.cast("GLvoid *",ffi.sizeof("GLfloat")*1*4))
	GL.glVertexAttribPointer(modelMatrix+2,4,GL.GL_FLOAT,0,ffi.sizeof("kmMat4"),ffi.cast("GLvoid *",ffi.sizeof("GLfloat")*2*4))
	GL.glVertexAttribPointer(modelMatrix+3,4,GL.GL_FLOAT,0,ffi.sizeof("kmMat4"),ffi.cast("GLvoid *",ffi.sizeof("GLfloat")*3*4))

	GL.glVertexAttribDivisor(modelMatrix+0,1)
	GL.glVertexAttribDivisor(modelMatrix+1,1)
	GL.glVertexAttribDivisor(modelMatrix+2,1)
	GL.glVertexAttribDivisor(modelMatrix+3,1)
	
--[[    	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,self.ibo[0])
	
 	local instanceID = gfx.shaders.getAttribLocation("instanceID")
	
	GL.glEnableVertexAttribArray(instanceID)
	
	GL.glVertexAttribIPointer(instanceID,1,GL.GL_UNSIGNED_INT,ffi.sizeof("GLuint")*5,ffi.cast("GLvoid *",ffi.sizeof("GLuint")*4))
	
	GL.glVertexAttribDivisor(instanceID,1) ]]
	
	GL.glBindVertexArray(0)
end


function cBufferGroup:recalculate()
	self.renderableCount = 0

	local v_off, e_off, i_off, m_off = 0, 0, 0, 0
	local vbuff_needed,mbuff_needed,ebuff_needed,ibuff_needed = 0, 0, 0, 0

	-- check if buffers are large enough
	for o_index=1,#renderable_objects do
		local obj = renderable_objects[o_index]
		
		if (obj.vertices and obj.indices) then
			vbuff_needed = vbuff_needed + v_off*ffi.sizeof("GLfloat") + ffi.sizeof(obj.vertices)
			mbuff_needed = mbuff_needed + m_off*ffi.sizeof("GLfloat") + 16*ffi.sizeof("GLfloat")
			ebuff_needed = ebuff_needed + e_off*ffi.sizeof("GLuint") + ffi.sizeof(obj.indices)
			ibuff_needed = ibuff_needed + i_off*ffi.sizeof("GLuint") + 5*ffi.sizeof("GLuint")
			
			v_off = v_off + ffi.sizeof(obj.vertices)/ffi.sizeof("GLfloat")
			e_off = e_off + ffi.sizeof(obj.indices)/ffi.sizeof("GLuint")
			i_off = i_off + 5
			m_off = m_off + 16
		end
	end
	
	if (self.vertex_buffer_alloc < vbuff_needed) then 
		self.vertex_buffer_alloc = utils.roundToMultiple(vbuff_needed,1024)
		GL.glNamedBufferData(self.vbo[0],self.vertex_buffer_alloc,nil,GL.GL_STREAM_DRAW)
		print("resized vertex buffer",self.vertex_buffer_alloc/1024,"KB")
	end 
	if (self.matrix_buffer_alloc < mbuff_needed) then 
		self.matrix_buffer_alloc = utils.roundToMultiple(mbuff_needed,1024)
		GL.glNamedBufferData(self.mbo[0],self.matrix_buffer_alloc,nil,GL.GL_STREAM_DRAW)
		print("resized matrix buffer",self.matrix_buffer_alloc/1024,"KB")
	end
	if (self.index_buffer_alloc < ebuff_needed) then
		self.index_buffer_alloc = utils.roundToMultiple(ebuff_needed,1024)
		GL.glNamedBufferData(self.ebo[0],self.index_buffer_alloc,nil,GL.GL_STREAM_DRAW)
		print("resized index buffer",self.index_buffer_alloc/1024,"KB")
	end
	if (self.indirect_buffer_alloc < ibuff_needed) then
		self.indirect_buffer_alloc = utils.roundToMultiple(ibuff_needed,1024)
		GL.glNamedBufferData(self.ibo[0],self.indirect_buffer_alloc,nil,GL.GL_STREAM_DRAW)
		print("resized indirect buffer",self.indirect_buffer_alloc/1024,"KB")
	end
	
	local vertices = GL.glMapNamedBufferRange(self.vbo[0],0,vbuff_needed,bit.bor(GL.GL_MAP_WRITE_BIT,GL.GL_MAP_UNSYNCHRONIZED_BIT))
	vertices = vertices ~= nil and ffi.cast("GLfloat *",vertices)
	
	local indices = GL.glMapNamedBufferRange(self.ebo[0],0,ebuff_needed,bit.bor(GL.GL_MAP_WRITE_BIT,GL.GL_MAP_UNSYNCHRONIZED_BIT))
	indices = indices ~= nil and ffi.cast("GLuint *",indices)
	
	local indirect = GL.glMapNamedBufferRange(self.ibo[0],0,ibuff_needed,bit.bor(GL.GL_MAP_WRITE_BIT,GL.GL_MAP_UNSYNCHRONIZED_BIT))
	indirect = indirect ~= nil and ffi.cast("GLuint *",indirect)

	local matrices = GL.glMapNamedBufferRange(self.mbo[0],0,mbuff_needed,bit.bor(GL.GL_MAP_WRITE_BIT,GL.GL_MAP_UNSYNCHRONIZED_BIT))
	matrices = matrices ~= nil and ffi.cast("GLfloat *",matrices)
	
	if not (matrices and vertices and indices and indirect) then
		return
	end
	
	v_off, e_off, i_off, m_off = 0, 0, 0, 0

	for o_index=1,#renderable_objects do
		local obj = renderable_objects[o_index]
		
		if (obj.vertices and obj.indices) then
			local mat4 = ffi.new("kmMat4")
			kazmath.kmMat4Identity(mat4)
			kazmath.kmMat4Translation(mat4,o_index*.2,0,0)

			ffi.copy(matrices+m_off,mat4.mat,ffi.sizeof(mat4.mat))
			ffi.copy(vertices+v_off,obj.vertices,ffi.sizeof(obj.vertices))
			ffi.copy(indices+e_off,obj.indices,ffi.sizeof(obj.indices))
			local buffer = indirect+i_off
			buffer[0] = ffi.sizeof(obj.indices)/ffi.sizeof("GLuint")
			buffer[1] = 1
			buffer[2] = e_off
			buffer[3] = v_off/14
			buffer[4] = self.renderableCount

			self.renderableCount = self.renderableCount + 1
			
			v_off = v_off + ffi.sizeof(obj.vertices)/ffi.sizeof("GLfloat")
			e_off = e_off + ffi.sizeof(obj.indices)/ffi.sizeof("GLuint")
			i_off = i_off + 5
			m_off = m_off + 16
		end
	end

	GL.glUnmapNamedBuffer(self.vbo[0])
	GL.glUnmapNamedBuffer(self.mbo[0])
	GL.glUnmapNamedBuffer(self.ebo[0])
	GL.glUnmapNamedBuffer(self.ibo[0])
end