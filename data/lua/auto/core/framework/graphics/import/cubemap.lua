local gfx = core.framework.graphics

-- core.framework.graphics.import.cubemap(type,tableoffnames)
getmetatable(this).__call = function (self,typ,fnames)
	local cmap = cCubemap(typ,fnames)
	return cmap
end

local faces = {
	right  = GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X,
	left   = GL.GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	top    = GL.GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
	bottom = GL.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
	front  = GL.GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
	back   = GL.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
}

Class "cCubemap"
function cCubemap:__init(typ,filenames)
	if (typ == "diffuse") then
		GL.glActiveTexture(GL.GL_TEXTURE1)
	elseif (typ == "specular") then
		GL.glActiveTexture(GL.GL_TEXTURE2)
	elseif (typ == "environment") then
		GL.glActiveTexture(GL.GL_TEXTURE0)
	end

	self.images = ffi.new("ILuint[?]",#filenames)
	IL.ilGenImages(#filenames,self.images)

	self.texture = ffi.new("GLuint[1]")
	GL.glGenTextures(1,self.texture)
	GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP,self.texture[0])

	local mipmapLevels = #filenames/6
	if (mipmapLevels < 2) then
		GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP,GL.GL_TEXTURE_MIN_FILTER,GL.GL_LINEAR)
		GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP,GL.GL_TEXTURE_MAG_FILTER,GL.GL_LINEAR)
	else
		GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP,GL.GL_TEXTURE_MIN_FILTER,GL.GL_LINEAR_MIPMAP_LINEAR)
		GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP,GL.GL_TEXTURE_MAG_FILTER,GL.GL_LINEAR)
	end

	self.pixels = {}

	for i, face in ipairs(filenames) do
		IL.ilBindImage(self.images[ i-1 ])

		local target = face[ 1 ]
		local filename = face[ 2 ]
		local level = math.ceil(i/6)-1
		local buffer, length = filesystem.read(filename)
		if (buffer == nil) then
			error(length,3)
		end
		IL.ilLoadL(IL.IL_TYPE_UNKNOWN,buffer,length)
		IL.ilConvertImage(IL.IL_RGBA,IL.IL_UNSIGNED_BYTE)
		local width = IL.ilGetInteger(IL.IL_IMAGE_WIDTH)
		local height = IL.ilGetInteger(IL.IL_IMAGE_HEIGHT)
		local pixels = IL.ilGetData()
		self.pixels[ i ] = pixels

		GL.glTexImage2D(
			faces[ target ],
			level,
			GL.GL_RGBA,
			width,
			height,
			0,
			GL.GL_RGBA,
			GL.GL_UNSIGNED_BYTE,
			pixels
		)
	end

	GL.glActiveTexture(GL.GL_TEXTURE0)
end

function cCubemap:__gc()
	GL.glDeleteTextures(1,self.texture)
	IL.ilDeleteImages(#self.pixels,self.images)
end
