aliases = nil
joyToAlias = {}

function on_game_start()
	core.config.set_default("keybinding","joystick",{
		default = {
		["pause"] 		= {1,true},
		["action"] 		= {2,true},
		["up"] 			= {3,true},
		["left"]		= {4,true},
		["down"] 		= {5,true},
		["right"] 		= {6,true}
		}
	})
	-- see SDL_JOYDEVICEADDED for config loading
end

-- TODO: pressed functions