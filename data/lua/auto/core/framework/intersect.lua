local DBL_EPSILON = 2.2204460492503131e-16 -- TODO: use FFI

-- http://stackoverflow.com/a/23976134/1190664
-- Test if ray intersects an infinite plane. Normal point is any point that is perpendicular to that plane.
function ray_plane(posX,posY,posZ,dirX,dirY,dirZ,planX,planY,planZ,normX,normY,normZ)

	local pos = ffi.new("kmVec3",posX,posY,posZ)
	local dir = ffi.new("kmVec3",dirX,dirY,dirZ)
	local plan = ffi.new("kmVec3",planX,planY,planZ)
	local norm = ffi.new("kmVec3",normX,normY,normZ)
	
	local e1 = ffi.new("kmVec3",0,0,0)
	local h = ffi.new("kmVec3",0,0,0)
	local s = ffi.new("kmVec3",0,0,0)
	
	local a = kazmath.kmVec3Dot(norm,dir)
	
	-- ray does not intersect plane
	if (math.abs(a) < DBL_EPSILON) then
		return false
	end

	-- distance of direction
	kazmath.kmVec3Subtract(e1,plan,pos)
	local t = kazmath.kmVec3Dot(e1,norm) / a

	if (t < DBL_EPSILON) then
		return false
	end

	-- Return collision point and distance from ray origin
	kazmath.kmVec3Scale(h,dir,t)
	kazmath.kmVec3Add(s,pos,h)
	return s.x,s.y,s.z,t
end

-- http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
-- Test if ray intersects a triangle defined by three points
function ray_triangle(posX,posY,posZ,dirX,dirY,dirZ,x1,y1,z1,x2,y2,z2,x3,y3,z3)

	local pos = ffi.new("kmVec3",posX,posY,posZ)
	local dir = ffi.new("kmVec3",dirX,dirY,dirZ)
	local tri1 = ffi.new("kmVec3",x1,y1,z1)
	local tri2 = ffi.new("kmVec3",x2,y2,z2)
	local tri3 = ffi.new("kmVec3",x3,y3,z3)
	
	local e1 = ffi.new("kmVec3",0,0,0)
	local e2 = ffi.new("kmVec3",0,0,0)
	local h = ffi.new("kmVec3",0,0,0)
	local s = ffi.new("kmVec3",0,0,0)
	local q = ffi.new("kmVec3",0,0,0)

	kazmath.kmVec3Subtract(e1,tri2,tri1)
	kazmath.kmVec3Subtract(e2,tri3,tri1)
	kazmath.kmVec3Cross(h,dir,e2)
	
	local a = kazmath.kmVec3Dot(h,e1)
	
	-- if a is too close to 0, ray does not intersect triangle
	if (math.abs(a) < DBL_EPSILON) then
		return false
	end

	local f = 1 / a
	kazmath.kmVec3Subtract(s,pos,tri1)
	local u = kazmath.kmVec3Dot(s,h) * f
	
	-- ray does not intersect triangle
	if (u < 0 or u > 1) then
		return false
	end

	kazmath.kmVec3Cross(q,s,e1)
	local v = kazmath.kmVec3Dot(dir,q) * f
	
	-- ray does not intersect triangle
	if (v < 0 or u + v > 1) then
		return false
	end

	-- at this stage we can compute t to find out where
	-- the intersection point is on the line
	local t = kazmath.kmVec3Dot(q,e2) * f

	-- ray does not intersect triangle
	if (t < DBL_EPSILON) then
		return false
	end
	
	-- return position of intersection
	kazmath.kmVec3Scale(h,dir,t)
	kazmath.kmVec3Add(s,pos,h)
	return s.x,s.y,s.z
end

-- Test if point inside an Axis-Aligned Bounding Box
function point_aabb(posX,posY,posZ,minX,minY,minZ,maxX,maxY,maxZ)
	return minX <= posX and maxX >= posX and minY <= posY and maxY >= posY and minZ <= posZ and maxZ >= posZ
end

-- Test if Axis-Aligned Bounding Box intersects with another Axis-Aligned Bounding Box
function aabb_aabb(minX1,minY1,minZ1,maxX1,maxY1,maxZ1,minX2,minY2,minZ2,maxX2,maxY2,maxZ2)
	return minX1 <= maxX2 and maxX1 >= minX2 and minY1 <= maxY2 and maxY1 >= minY2 and minZ1 <= maxZ2 and maxZ1 >= minZ2
end

-- http://gamedev.stackexchange.com/a/18459
-- Test if ray intersects with Axis-Aligned Bounding Box. Direction must be normalized first.
function ray_aabb(posX,posY,posZ,dirX,dirY,dirZ,minX,minY,minZ,maxX,maxY,maxZ)
	dirX = 1 / dirX
	dirY = 1 / dirY
	dirZ = 1 / dirZ

	local t1 = (minX - posX) * dirX
	local t2 = (maxX - posX) * dirX
	local t3 = (minY - posY) * dirY
	local t4 = (maxY - posY) * dirY
	local t5 = (minZ - posZ) * dirZ
	local t6 = (maxZ - posZ) * dirZ

	local tmin = math.max(math.max(math.min(t1, t2), math.min(t3, t4)), math.min(t5, t6))
	local tmax = math.min(math.min(math.max(t1, t2), math.max(t3, t4)), math.max(t5, t6))

	-- ray is intersecting AABB, but whole AABB is behind us
	if (tmax < 0) then
		return false
	end

	-- ray does not intersect AABB
	if (tmin > tmax) then
		return false
	end

	local pos = ffi.new("kmVec3",posX,posY,posZ)
	local dir = ffi.new("kmVec3",dirX,dirY,dirZ)
	local sca = ffi.new("kmVec3")
	local out = ffi.new("kmVec3")
	
	kazmath.kmVec3Scale(sca,dir,tmin)
	kazmath.kmVec3Add(out,sca,pos)

	-- Return collision point and distance from ray origin
	return out.x,out.y,out.z,tmin
end