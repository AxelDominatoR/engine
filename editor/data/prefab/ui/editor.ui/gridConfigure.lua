function editor_grid_configure_win_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_grid_configure_win") then
		local w,h = core.framework.graphics.getSize()
		p.x = w/2-p.w/2
		p.y = h/2-p.h/2
		-- revert on cancel
		p.revert = {}
		p.revert.width = editor.grid2DWidth or 0
		p.revert.height = editor.grid2DHeight or 0
	elseif (node.id == "grid_configure_width") then
		p.text = ffi.new("char[?]",255,tostring(editor.grid2DWidth))
	elseif (node.id == "grid_configure_height") then
		p.text = ffi.new("char[?]",255,tostring(editor.grid2DHeight))
	end
end

function editor_grid_configure_close(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_grid_configure_win") then
		data[node.id] = nil -- to reinit when shown again
	end
end

function editor_grid_configure_submit(ctx,node,data,depth)
	if (node.id == "grid_configure_btn_save") then
		editor.grid2DWidth = tonumber(ffi.string(data.grid_configure_width.text)) or 0
		editor.grid2DHeight = tonumber(ffi.string(data.grid_configure_height.text)) or 0
	elseif (node.id == "grid_configure_btn_cancel") then
		editor.grid2DHeight = p.revert.height
		editor.grid2DWidth = p.revert.width
	end
	data.__info.ui_grid_configure = nil
end

function editor_grid_configure_update(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "grid_configure_width") then
		editor.grid2DWidth = tonumber(ffi.string(p.text)) or 0
	elseif (node.id == "grid_configure_height") then
		editor.grid2DHeight = tonumber(ffi.string(p.text)) or 0
	end
end