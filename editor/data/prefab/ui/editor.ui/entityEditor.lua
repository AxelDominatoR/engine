function entity_editor_win_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_entity_editor_win") then
		local w,h = core.framework.graphics.getSize()
		p.x = w/2-p.w/2
		p.y = h/2-p.h/2
		-- revert on cancel
		local ent = selectedID and core.entity.byID(selectedID)
		if (ent) then
			p.component_undo = {}
			for name,o in pairs(ent.component) do
				if (o.stateWrite) then
					p.component_undo[name] = {}
					o:stateWrite(p.component_undo[name])
				end
			end
		end
	elseif (node.id == "editor_entity_component_editor_win") then
		local w,h = core.framework.graphics.getSize()
		p.x = w/2-p.w/2
		p.y = h/2-p.h/2
	elseif (node.id == "edit_entity_alias") then
		local ent = selectedID and core.entity.byID(selectedID)
		if (ent) then
			p.text = ffi.new("char[?]",255,ent.alias)
		end
	end
end

function entity_editor_win_close(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_entity_editor_win") then
		data.__info.ui_entity_editor = nil
		data.__info.ui_entity_component_editor = nil
		core.interface.api.clearData(data) -- to allow reinit
	end
end

function entity_editor_submit(ctx,node,data,depth)
	if (node.id == "entity_editor_btn_components") then
		data.__info.ui_entity_component_editor = true
		return
	end
	local ent = selectedID and core.entity.byID(selectedID)
	if (ent) then
		if (node.id == "entity_editor_btn_cancel") then
			if (data.editor_entity_editor_win.component_undo) then
				for name,o in pairs(ent.component) do
					if (o.stateRead and data.editor_entity_editor_win.component_undo[name]) then
						o:stateRead(data.editor_entity_editor_win.component_undo[name])
					end
				end
			end
		elseif (node.id == "entity_editor_btn_save") then
			ent.alias = utils.trim(ffi.string(data.edit_entity_alias.text))
			for name,t in pairs(ent.component) do
				if (core.entity.component[name].entity_editor_submit) then
					core.entity.component[name].entity_editor_submit(ent,t,ctx,node,data,depth)
				end
			end
		end
	end
	data.__info.ui_entity_editor = nil
	data.__info.ui_entity_component_editor = nil
	core.interface.api.clearData(data) -- to allow reinit
end

function action_entity_component_edit(ctx,node,data,depth)
	local ent = selectedID and core.entity.byID(selectedID)
	if (ent) then
		if (node.id == "btn_component_add") then 
			local p = data.inactive_components.components
			if (p) then
				for name,v in pairs(p) do
					if (v[0] == 1) then
						v[0] = 0
						ent:addComponent(name)
					end
				end
			end
		elseif (node.id == "btn_component_remove") then
			local p = data.active_components.components
			if (p) then
				for name,v in pairs(p) do
					if (v[0] == 1) then
						v[0] = 0
						ent:removeComponent(name)
					end
				end
			end
		end
	end
end