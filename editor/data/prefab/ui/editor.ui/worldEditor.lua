function editor_make_world_win_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_make_world_win") then
		local w,h = core.framework.graphics.getSize()
		p.x = w/2-p.w/2
		p.y = h/2-p.h/2
	end
end

function editor_make_world_win_close(ctx,node,data,depth)
	if (node.id == "editor_make_world_win") then
		data.__info.ui_make_world = nil
		data[node.id] = nil -- to reinit when shown again
	end
end

function action_make_world(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "menu_file_tree_world_make") then 
		p.scenes = {}
		data.__info.ui_make_world = true
	elseif (node.id == "btn_scene_add") then
		if (data.inactive_scenes and data.inactive_scenes.scenes) then
			for name,v in pairs(data.inactive_scenes.scenes) do
				if (v[0] == 1) then
					v[0] = 0
					data.menu_file_tree_world_make.scenes[name] = true
				end
			end
		end
	elseif (node.id == "btn_scene_remove") then 
		if (data.menu_file_tree_world_make and data.menu_file_tree_world_make.scenes and data.active_scenes) then
			for name,v in pairs(data.menu_file_tree_world_make.scenes) do
				if (data.active_scenes.scenes[name][0] == 1) then
					data.active_scenes.scenes[name][0] = 0
					data.menu_file_tree_world_make.scenes[name] = nil
				end
			end
		end
	elseif (node.id == "editor_make_world_btn_export") then
		if not (table.isempty(data.menu_file_tree_world_make.scenes)) then
			if (core.interface.import.prefab.exists("saveWorld")) then 
				return
			end
			local on_select = function(fullpath)
				editor.world.save(fullpath,data.menu_file_tree_world_make.scenes)
			end
			core.interface.import.prefab("saveWorld","prefab/ui/filebrowser.ui",{ directory="prefab/world",filter={"world"}, browserType="SaveAs", onSelect=on_select })
		end
	end
end 

function list_inactive_scenes(ctx,node,data,depth)
	nuklear.nk_layout_row_dynamic(ctx,25,1)
	local p = data[node.id]
	if not (p.scenes) then
		p.scenes = {}
		filesystem.fileForEach("prefab/scene",true,{"scene"},function(path,fname,fullpath)
			local name = utils.trim_ext(fname)
			p.scenes[name] = ffi.new("int[1]")
		end)		
	end
	
	for name,v in pairs(p.scenes) do
		if (data.menu_file_tree_world_make.scenes and data.menu_file_tree_world_make.scenes[name] == nil) then
			nuklear.nk_selectable_label(ctx,name,nuklear.NK_TEXT_LEFT,v)
		end
	end
end 

function list_active_scenes(ctx,node,data,depth)
	nuklear.nk_layout_row_dynamic(ctx,25,1)
	local p = data[node.id]
	if not (p.scenes) then
		p.scenes = {}
	end
	for name,t in pairs(data.menu_file_tree_world_make.scenes) do
		if not (p.scenes[name]) then 
			p.scenes[name] = ffi.new("int[1]")
		end
		nuklear.nk_selectable_label(ctx,name,nuklear.NK_TEXT_LEFT,p.scenes[name])
	end
end