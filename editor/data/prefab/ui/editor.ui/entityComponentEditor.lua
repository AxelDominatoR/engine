function entity_component_editor_win_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_entity_component_editor_win") then
		local w,h = core.framework.graphics.getSize()
		p.x = w/2-p.w/2
		p.y = h/2-p.h/2
	end
end

function entity_component_editor_win_close(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_entity_component_editor_win") then
		data.__info.ui_entity_component_editor = nil
		core.interface.api.clearData(data) -- to allow reinit
	end
end

function entity_components_tree(ctx,node,data,depth)
	local ent = selectedID and core.entity.byID(selectedID)
	if (ent) then
		for name,t in pairs(ent.component) do
			if (core.entity.component[name].editor_controls and nuklear.nk_tree_push_hashed(ctx,nuklear.NK_TREE_TAB,name,nuklear.NK_MAXIMIZED,name,#name,0) == 1) then
				if (core.entity.component[name].editor_controls) then
					core.entity.component[name].editor_controls(ent,t,ctx,node,data,depth)
				end
				nuklear.nk_tree_pop(ctx)
			end
		end
	end
end

function list_inactive_components(ctx,node,data,depth)
	local ent = selectedID and core.entity.byID(selectedID)
	if (ent) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local p = data[node.id]
		if not (p.components) then
			p.components = {}
			filesystem.fileForEach("lua/auto/core/entity/component",true,{"lua"},function(path,fname,fullpath)
				local name = fname:sub(1,-5)
				p.components[name] = ffi.new("int[1]")
			end)		
		end
		
		for name,v in pairs(p.components) do
			if not (ent.component[name]) then
				nuklear.nk_selectable_label(ctx,name,nuklear.NK_TEXT_LEFT,v)
			end
		end
	end
end 

function list_active_components(ctx,node,data,depth)
	local ent = selectedID and core.entity.byID(selectedID)
	if (ent) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local p = data[node.id]
		if not (p.components) then
			p.components = {}
		end
		for name,t in pairs(ent.component) do
			if not (p.components[name]) then 
				p.components[name] = ffi.new("int[1]")
			end
			nuklear.nk_selectable_label(ctx,name,nuklear.NK_TEXT_LEFT,p.components[name])
		end
	end
end